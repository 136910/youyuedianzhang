﻿using System;
using System.Collections.Generic;
using System.Text;
using UploadService.Base;
using UploadService.Option;

namespace SuperShopInfrastructure.FileUploader
{
    public class CqProductUploader : MinDimensionUploader
    {
        public CqProductUploader(ImageServiceOption option) : base(option) { }

        public override int MinWidth
        {
            get { return 10; }
        }

        public override int MinHeight
        {
            get { return 10; }
        }

        /// <summary>
        /// 50
        /// </summary>
        public override int MaxBytesLength
        {
            get { return 5000 * 1024; }
        }

        public override string[] AllowedImageTypes
        {
            get { return new string[] { ".jpg", ".png", ".jpeg" }; }
        }

        public override string FolderName
        {
            get
            {
                return "CQ_Product";
                //    ConfigSettings.Instance.FileUploadFolderNameAd;
            }
        }
    }
}
