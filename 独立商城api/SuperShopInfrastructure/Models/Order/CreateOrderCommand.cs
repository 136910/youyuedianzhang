﻿using SuperShopInfrastructure.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SuperShopInfrastructure.Models.Order
{
    public class CreateOrderCommand //deleted
    {
        [Required]
        [Decrypt]
        public string Spid { get; set; }
        [Required]
        [Decrypt]
        public string Said { get; set; }
        [Required]
        [Decrypt]
        public string UserId { get; set; }

        public int ConsigneeId { get; set; }
        public string Remarks { get; set; }
        public int CouponId { get; set; }
        public decimal ShipAmount { get; set; }
        public decimal ToalAmount { get; set; }
        public List<CreateOrder_ProductItem> CreateOrder_ProductItems { get; set; }
        public decimal ProductAmount { get; set; }
        public decimal CouponAmount { get; set; }
    }

    public class CreateOrder_ProductItem
    {
        public string ProductId { get; set; }
        public int Product_SkuId { get; set; }
        public int SaleNum { get; set; }
        public CreaterOrder_CheckItem CreaterOrder_CheckItem { get; set; }
    }
    public class CreaterOrder_CheckItem
    {
        public int ProductSkuId { get; set; }
        public int StockNum { get; set; }
        public int IsDel { get; set; }
        public int Product_OptionStatus { get; set; }
        public int Product_IsDel { get; set; }
        public string ProductName { get; set; }
        public string ProppetyCombineName { get; set; }
        public decimal SallPrice { get; set; }
        public string SkuCode { get; set; }
        public string PropetyCombineId { get; set; }
        public float Weight { get; set; }

    }
}
