﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperShopInfrastructure.Models.Order
{

    public class CreateOrderResultViewModel
    {
        public OrderPayResultModel PayParamater { get; set; }
        /// <summary>
        /// int ,0：积分，1：微信支付,2:银行分期付款
        /// </summary>
        public int PayType { get; set; }

        public long OrderID { get; set; }
    }

    public class OrderPayResultModel
    {
        public string AppId { get; set; }
        public string TimeStamp { get; set; }
        public string NonceStr { get; set; }
        public string Package { get; set; }
        public string SignType { get; set; }
        public string PaySign { get; set; }

        public string Payorderno { get; set; }

        public string Orderno { get; set; }
        public long OrderID { get; set; }
    }

    public class Program_CreateOrder_CartResult//deleted
    {
        public string OrderNumber { get; set; }
        /// <summary>
        /// 支付key
        /// </summary>
        public string Paykey { get; set; }
        /// <summary>
        /// 应用APPID
        /// </summary>
        public string Appid { get; set; }
        /// <summary>
        /// 支付商户号
        /// </summary>
        public string Mch_id { get; set; }
        /// <summary>
        /// 用户openid
        /// </summary>
        public string Openid { get; set; }
        public double TotalFee { get; set; }
        public string ProductBody { get; set; }

        public int OrderID { get; set; }
    }
}
