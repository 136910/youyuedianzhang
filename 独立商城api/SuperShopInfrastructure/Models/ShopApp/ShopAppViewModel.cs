﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperShopInfrastructure.Models.ShopApp
{
    public class ShopAppViewModel//deleted
    {
        /// <summary>
        /// 店铺ID
        /// </summary>
        public string Id { get; set; }
        public string AppId { get; set; }
        public string AppSecret { get; set; }
        public string PaymentId { get; set; }
        public string PaySecret { get; set; }
        public string CreateTime { get; set; }
        public string UpdateTime { get; set; }
        /// <summary>
        /// 店主用户ID
        /// </summary>
        public string ShopAdminId { get; set; }
        public string ServiceQQ { get; set; }
        public string ServiceWx { get; set; }
        public string ServicePhone { get; set; }
        public string ShopName { get; set; }
        public string LoginName { get; set; }

        /// <summary>
        /// 店铺加密ID
        /// </summary>
        public string SecretId { get; set; }
        public int OptionStatus { get; set; }
        public string Remark { get; set; }

    }
}
