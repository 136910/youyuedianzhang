﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperShopInfrastructure.Models.Comment
{
    public class CommentViewModel
    {
        public int Id { get; set; }
        public string NickName { get; set; }
        public string HeadImgUrl { get; set; }
        public int CommentLevel { get; set; }
        public string CommentMsg { get; set; }
        public string CreateTime { get; set; }
    }
}
