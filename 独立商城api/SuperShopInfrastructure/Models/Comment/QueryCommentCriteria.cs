﻿using SuperShopInfrastructure.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SuperShopInfrastructure.Models.Comment
{
    public class QueryCommentCriteria: BasePagedModel
    {
        [Required]
        [Decrypt]
        public string ProductId { get; set; }
    }
}
