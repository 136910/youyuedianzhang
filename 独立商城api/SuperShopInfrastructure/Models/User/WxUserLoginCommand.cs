﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperShopInfrastructure.Models.User
{
    public class WxUserLoginCommand//deleted
    {
        public string Spid { get; set; }
        public string Code { get; set; }
        public string HeadImgUrl { get; set; }
        public int Sex { get; set; }
        public string NickName { get; set; }
        public string OpenId { get; set; }
    }
}
