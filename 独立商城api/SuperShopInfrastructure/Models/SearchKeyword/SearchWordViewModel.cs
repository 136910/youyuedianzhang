﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperShopInfrastructure.Models.SearchKeyword
{
    public class SearchWordViewModel
    {
        public int Id { get; set; }
        public string KeyWord { get; set; }
    }
}
