﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperShopInfrastructure.Models.ShopCart
{
    public class ShopCartListViewModel
    {
        public string ProductId { get; set; }
        public int Product_SkuId { get; set; }
        public int SaleNum { get; set; }
        public string ProductName { get; set; }
        public string Brand { get; set; }
        public decimal SallPrice { get; set; }
        public int StockNum { get; set; }
        public string ProppetyCombineName { get; set; }
        public int SkuType { get; set; }
        public string ImgPath { get; set; }
        public int IsDel { get; set; }
        public float Weight { get; set; }
        public int ShippingTemplateId { get; set; }
    }
}
