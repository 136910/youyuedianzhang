﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperShopInfrastructure.Models.Product
{
    public class MenuItemViewModel
    {
        public int Id { get; set; }
        public string MenuName { get; set; }
        public string ImgPath { get; set; }
        public int IsAll { get; set; }
    }
}
