﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperShopInfrastructure.Models.Product
{
    public class QueryProductListCriteria
    {
        public string Spid { get; set; }
        public string keyWord { get; set; }
        public int OffSet { get; set; }
        public int Size { get; set; }
        public int MenuId { get; set; }
    }
}
