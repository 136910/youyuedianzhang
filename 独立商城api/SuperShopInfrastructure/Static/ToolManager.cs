﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace SuperShopInfrastructure.Static
{
    public static class ToolManager
    {
        /// <summary>
        /// 根据ecpm和播放次数，计算收益
        /// </summary>
        /// <param name="ecpm"></param>
        /// <param name="playCount"></param>
        /// <returns></returns>
        public static string GetAmount(decimal ecpm, long playCount)
        {
            var res = "计算中...";
            if (Convert.ToInt32(ecpm) == 0)
                return res;
            res = Math.Round(ecpm * playCount / 1000, 2).ToString() + "元";
            return res;
        }


        /// <summary>
        /// 计时器开始
        /// </summary>
        /// <returns></returns>
        public static Stopwatch TimerStart()
        {
            Stopwatch watch = new Stopwatch();
            watch.Reset();
            watch.Start();
            return watch;
        }
        /// <summary>
        /// 生成短token
        /// </summary>
        /// <returns></returns>
        public static string CreateShortToken()
        {
            var rnd = new Random();
            var tokenData = new byte[8];
            rnd.NextBytes(tokenData);
            var token = Convert.ToBase64String(tokenData).Replace("=", "").Replace("+", "").Replace("/", "");
            return token;
        }
        /// <summary>
        /// 隐藏手机号中间4位
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        public static string SecretPhone(string phone)
        {
            return Regex.Replace(phone, "(\\d{3})\\d{4}(\\d{4})", "$1****$2");
        }

        /// <summary>
        /// 计时器结束
        /// </summary>
        /// <param name="watch"></param>
        /// <returns></returns>
        public static string TimerEnd(Stopwatch watch)
        {
            watch.Stop();
            double costtime = watch.ElapsedMilliseconds;
            return costtime.ToString();
        }

        /// <summary>
        /// 自动生成编号  201008251145409865
        /// </summary>
        /// <returns></returns>
        public static string CreateNo()
        {
            Random random = new Random();
            string strRandom = random.Next(1000, 10000).ToString(); //生成编号 
            string code = DateTime.Now.ToString("yyyyMMddHHmmss") + strRandom;//形如
            return code;
        }

        /// <summary>
        /// 生成0-9随机数
        /// </summary>
        /// <param name="codeNum">生成长度</param>
        /// <returns></returns>
        public static string RndNum(int codeNum)
        {
            StringBuilder sb = new StringBuilder(codeNum);
            Random rand = new Random();
            for (int i = 1; i < codeNum + 1; i++)
            {
                int t = rand.Next(9);
                sb.AppendFormat("{0}", t);
            }
            return sb.ToString();

        }
        /// <summary>
        /// 删除最后结尾的一个逗号
        /// </summary>
        public static string DelLastComma(string str)
        {
            return str.Substring(0, str.LastIndexOf(","));
        }
        /// <summary>
        /// 删除最后结尾的指定字符后的字符
        /// </summary>
        public static string DelLastChar(string str, string strchar)
        {
            return str.Substring(0, str.LastIndexOf(strchar));
        }
        /// <summary>
        /// 删除最后结尾的长度
        /// </summary>
        /// <param name="str"></param>
        /// <param name="Length"></param>
        /// <returns></returns>
        public static string DelLastLength(string str, int Length)
        {
            if (string.IsNullOrEmpty(str))
                return "";
            str = str.Substring(0, str.Length - Length);
            return str;
        }

        /// <summary>
        /// 根据枚举的值获取枚举名称
        /// </summary>
        /// <typeparam name="T">枚举类型</typeparam>
        /// <param name="status">枚举的值</param>
        /// <returns></returns>
        public static string GetEnumName<T>(this int status)
        {
            return Enum.GetName(typeof(T), status);
        }

        public static string DecryptParms(string secret)
        {
            var res = 0;
            if (int.TryParse(SecretClass.DecryptQueryString(secret), out res))
            {
                return res.ToString();
            }
            return null;
        }

        /// <summary>
        /// datetime转换为unixtime
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        public static string ConvertDateTimeInt(System.DateTime time)
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1));
            return Convert.ToInt64((time - startTime).TotalSeconds).ToString();
        }

        /// <summary>
        /// 生成随机串 随机串包含字母或数字
        /// </summary>
        /// <returns></returns>
        public static string GenerateNonceStr()
        {
            return Guid.NewGuid().ToString().Replace("-", "");
        }
        /// <summary>
        /// 检测sql注入
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static bool IsSqlin(string str)
        {
            const string Sqlint = "'、;、exec、insert、select、delete、update、count、declare、script、;、drop、truncate";
            string[] sqlArray = Sqlint.Split('、');
            for (var i = 0; i < sqlArray.Length; i++)
            {
                if (str.ToLower().IndexOf(sqlArray[i]) != -1)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool IsSqlin(List<string> strs)
        {
            var res = false;
            const string Sqlint = "'、;、exec、insert、select、delete、update、count、declare、script、;、drop、truncate";
            string[] sqlArray = Sqlint.Split('、');
            foreach (var str in strs)
            {
                for (var i = 0; i < sqlArray.Length; i++)
                {
                    if (str.ToLower().IndexOf(sqlArray[i]) != -1)
                    {
                        res = true;
                        break;
                    }
                }
            }
            return res;
        }



        /// <summary>
        /// 判断当前会话是否是来自手机
        /// </summary>
        /// <returns></returns>



        /// <summary>
        /// 获取枚举名称集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static string[] GetNamesArr<T>()
        {
            return Enum.GetNames(typeof(T));
        }
        /// <summary>
        /// 将枚举转换成字典集合
        /// </summary>
        /// <typeparam name="T">枚举类型</typeparam>
        /// <returns></returns>
        public static Dictionary<string, int> getEnumDic<T>()
        {

            Dictionary<string, int> resultList = new Dictionary<string, int>();
            Type type = typeof(T);
            var strList = GetNamesArr<T>().ToList();
            foreach (string key in strList)
            {
                string val = Enum.Format(type, Enum.Parse(type, key), "d");
                resultList.Add(key, int.Parse(val));
            }
            return resultList;
        }
        /// <summary>
        /// 将枚举，转换成字典<描述和值>
        /// </summary>
        public static Dictionary<string, int> ForEnum<T>()
        {
            Dictionary<string, int> enumDic = new Dictionary<string, int>();
            Type t = typeof(T);
            Array arrays = Enum.GetValues(t);
            for (int i = 0; i < arrays.LongLength; i++)
            {
                T test = (T)arrays.GetValue(i);
                FieldInfo fieldInfo = test.GetType().GetField(test.ToString());
                object[] attribArray = fieldInfo.GetCustomAttributes(false);
                EnumDescriptionAttribute attrib = (EnumDescriptionAttribute)attribArray[0];
                enumDic.Add(attrib.Description, Convert.ToInt32(test));
            }
            return enumDic;
        }
        /// <summary>
        /// 获取指定枚举的描述
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetDescription(this Enum value)
        {
            FieldInfo field = value.GetType().GetField(value.ToString());
            EnumDescriptionAttribute attribute = Attribute.GetCustomAttribute(field, typeof(EnumDescriptionAttribute)) as EnumDescriptionAttribute;

            return attribute == null ? value.ToString() : attribute.Description;
        }

        /// <summary>
        /// 将阅读数转换成XX万
        /// </summary>
        /// <param name="readCount"></param>
        /// <returns></returns>
        public static string GetReadCountFomat(int readCount)
        {
            var result = string.Empty;
            if (readCount < 10000)
            {
                result = readCount.ToString();
            }
            else
            {
                result = string.Format("{0}万", Math.Round((double)readCount / (double)10000, 1)).ToString();
            }
            return result;
        }

        /// <summary>
        /// 将阅读数转换成XX万
        /// </summary>
        /// <param name="readCount"></param>
        /// <returns></returns>
        public static string GetReadCountFomat(long readCount)
        {
            var result = string.Empty;
            if (readCount < 10000)
            {
                result = readCount.ToString();
            }
            else
            {
                result = string.Format("{0}万", Math.Round((double)readCount / (double)10000, 1)).ToString();
            }
            return result;
        }

        /// <summary>
        /// 时间戳转几小时前
        /// </summary>
        /// <param name="timeStamp"></param>
        /// <returns></returns>
        public static string GetFomatTime(string timeStamp)
        {
            return DateStringFromNow(GetTime(timeStamp));
        }


        /// 时间戳转为C#格式时间  
        /// </summary>  
        /// <param name=”timeStamp”></param>  
        /// <returns></returns>  
        public static DateTime GetTime(string timeStamp)
        {
            DateTime dtStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            long lTime = long.Parse(timeStamp + "0000000");
            TimeSpan toNow = new TimeSpan(lTime); return dtStart.Add(toNow);
        }

        /// <summary>
        /// 将时间转成几小时前。。。。。
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string DateStringFromNow(DateTime dt)
        {
            TimeSpan span = DateTime.Now - dt;
            if (span.TotalDays > 60)
            {
                return dt.ToShortDateString();
            }
            else
            {
                if (span.TotalDays > 30)
                {
                    return
                    "1个月前";
                }
                else
                {
                    if (span.TotalDays > 14)
                    {
                        return
                        "2周前";
                    }
                    else
                    {
                        if (span.TotalDays > 7)
                        {
                            return
                            "1周前";
                        }
                        else
                        {
                            if (span.TotalDays > 1)
                            {
                                return
                                string.Format("{0}天前", (int)Math.Floor(span.TotalDays));
                            }
                            else
                            {
                                if (span.TotalHours > 1)
                                {
                                    return
                                    string.Format("{0}小时前", (int)Math.Floor(span.TotalHours));
                                }
                                else
                                {
                                    if (span.TotalMinutes > 1)
                                    {
                                        return
                                        string.Format("{0}分钟前", (int)Math.Floor(span.TotalMinutes));
                                    }
                                    else
                                    {
                                        if (span.TotalSeconds >= 1)
                                        {
                                            return
                                            string.Format("{0}秒前", (int)Math.Floor(span.TotalSeconds));
                                        }
                                        else
                                        {
                                            return
                                            "1秒前";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
