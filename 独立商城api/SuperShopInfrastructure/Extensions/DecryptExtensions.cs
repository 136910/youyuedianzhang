﻿using SuperShopInfrastructure.Attributes;
using SuperShopInfrastructure.Static;
using System;
using System.Reflection;

namespace SuperShopInfrastructure.Extensions
{
    public static class DecryptExtensions
    {
        public static string DecryptProperty<Entity>(this Entity entity) where Entity : class
        {
            string errorMsg = string.Empty;
            Type type = entity.GetType();
            PropertyInfo[] properties = type.GetProperties();
            foreach (var item in properties)
            {
                if (item.IsDefined(typeof(DecryptAttribute)))
                {
                    var vue = item.GetValue(entity);
                    if (vue == null || string.IsNullOrWhiteSpace(vue.ToString()))
                    {
                        errorMsg = $"{item.Name}不能为NULL";
                        break;
                    }

                    //解密赋值
                    var _res = ToolManager.DecryptParms(vue.ToString());
                    if (string.IsNullOrEmpty(_res))
                    {
                        errorMsg = $"{item.Name}参数异常";
                        break;
                    }

                    item.SetValue(entity, _res);
                }
            }
            return errorMsg;
        }

        
    }
}
