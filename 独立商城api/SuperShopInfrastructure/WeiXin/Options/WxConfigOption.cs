﻿namespace SuperShopInfrastructure.Weixin.Options
{
    public class WxConfigOption
    {
        /// <summary>
        /// 数据库读取，暂时不用(小程序appid)
        /// </summary>
        public string APPID { get; set; }
        /// <summary>
        /// 同上，密钥
        /// </summary>
        public string APP_Secret { get; set; }
        /// <summary>
        /// 同上，商户支付号
        /// </summary>
        public string Merchant_Id { get; set; }
        /// <summary>
        /// 同上，商户密钥
        /// </summary>
        public string PaySecret { get; set; }
        /// <summary>
        /// 服务号
        /// </summary>
        public string SToken { get; set; }
        public string WeixinAeskey { get; set; }
        /// <summary>
        /// 微信统一下单地址
        /// </summary>
        public string WxPayUrl { get; set; }
        /// <summary>
        /// 微信支付回调我们的URL地址
        /// </summary>
        public string WxCallBackUrl { get; set; }//deleted
    }
}
