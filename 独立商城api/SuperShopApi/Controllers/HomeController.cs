﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuperShopInfrastructure.Extensions;
using SuperShopInfrastructure.Models.Product;
using SuperShopInfrastructure.Static;
using SuperShopService.IServices;

namespace SuperShopApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IProductService productService;

        public HomeController(IProductService productService)
        {
            this.productService = productService;
        }

        /// <summary>
        /// 获取首页商品数据
        /// </summary>
        /// <param name="spid"></param>
        /// <param name="said"></param>
        /// <returns></returns>
        [HttpGet("data")]
        [ProducesResponseType(typeof(HomeProductDataViewModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetHomeData([FromQuery] string spid,[FromQuery] string said)
        {
            var _spid = ToolManager.DecryptParms(spid);
            if (_spid == null)
                return BadRequest("非法参数");
            var _said = ToolManager.DecryptParms(said);
            if (_said == null)
                return BadRequest("非法参数");
            var res = await productService.GetHomeData(_spid.ToInt(),_said.ToInt());
            return Ok(res);
        }
    }
}