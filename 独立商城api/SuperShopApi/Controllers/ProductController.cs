﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuperShopInfrastructure.Extensions;
using SuperShopInfrastructure.Models.Product;
using SuperShopInfrastructure.Static;
using SuperShopService.IServices;

namespace SuperShopApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService productService;
        private readonly ISetService setService;
        private readonly IShopAppService shopAppService;

        public ProductController(IProductService productService, ISetService setService, IShopAppService shopAppService)
        {
            this.productService = productService;
            this.setService = setService;
            this.shopAppService = shopAppService;
        }

        /// <summary>
        /// 获取目录列表
        /// </summary>
        /// <param name="said"></param>
        /// <returns></returns>
        [HttpGet("menuList")]
        [ProducesResponseType(typeof(List<MenuItemViewModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetMenuList([FromQuery] string said)
        {
            string _said = ToolManager.DecryptParms(said);
            if (string.IsNullOrEmpty(_said))
                return BadRequest("非法参数");
            var result = await productService.GetMenuList(_said.ToInt());
            return Ok(result);
        }

        /// <summary>
        /// 获取商品集合数据
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [HttpPost("list")]
        [ProducesResponseType(typeof(List<ProductListViewModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetProductList([FromBody] QueryProductListCriteria criteria)
        {
            string _spid = ToolManager.DecryptParms(criteria.Spid);
            if (_spid == null)
                return BadRequest("非法参数");
            criteria.Spid = _spid;
            var res = await productService.QueryProductList(criteria);
            return Ok(res);
        }

        /// <summary>
        /// 获取商品详情
        /// </summary>
        /// <param name="id"></param>
        /// <param name="said"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(ProductDetailViewModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetProductDetail([FromRoute] string id, [FromQuery] string said)
        {
            string _id = ToolManager.DecryptParms(id);
            if (_id == null)
                return BadRequest("非法参数");
            string _shopAdminId = ToolManager.DecryptParms(said);
            if (_shopAdminId == null)
                return BadRequest("非法参数");
            //读取商品主体
            var res = await productService.GetProduct(_id.ToInt());
            if (res == null)
                return BadRequest("数据错误");
            //读运费
            var shipmodel = await setService.GetShippingTemplateModel(res.ShipTemplateId);
            //取出运费所有区域的子运费模板的最大金额和最小金额
            var _minAmount = shipmodel.Items.Min(p => p.FirstAmount);
            var _maxAmount = shipmodel.Items.Max(p => p.FirstAmount);
            res.ShipAmount = _minAmount + " - " + _maxAmount;
            //根据商品主体的sku类型，读取商品的sku数据
            if (res.CurrentSkuType == 1)
            {
                res.SkuManagerModel = await productService.GetProductSkuManagerModel(_shopAdminId.ToInt(), _id);//由于库存原因，商品SKU不走redis
            }
            else if (res.CurrentSkuType == 2)
            {
                res.SkuManagerModel = await productService.GetProductSkuManagerModel_AJAX(_shopAdminId.ToInt(), _id);
            }
            return Ok(res);
        }

        /// <summary>
        /// 商品专场列表
        /// </summary>
        /// <param name="said"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [HttpGet("specialPlaces")]
        [ProducesResponseType(typeof(List<SpecialPlaceViewModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetSpecialPlaceData([FromQuery] string said,[FromQuery] int type)
        {
            string _shopAdminId = ToolManager.DecryptParms(said);
            if (_shopAdminId == null)
                return BadRequest("非法参数");
            //读取商品主体
            var res = await productService.SpecialPlaceList(_shopAdminId.ToInt(),type);
            return Ok(res);
        }

        /// <summary>
        /// 生成商品详情海报
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("make/qrImg")]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> MobileQrCodeImage([FromBody] MakeProductQrImageCommand command)
        {
            string _shopId = ToolManager.DecryptParms(command.ShopID);
            if (_shopId == null)
                return BadRequest("非法参数");
            command.ShopID = _shopId;
            var res = await productService.MakeOroductQrImage(command);
            if (!res.IsSuccess)
                return BadRequest(res.FailureReason);
            return Ok(res.GetData());
        }

    }
}