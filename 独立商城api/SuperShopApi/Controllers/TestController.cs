﻿using System.Threading.Tasks;
using CqBuiltinService.Controller;
using Microsoft.AspNetCore.Mvc;
using SuperShopInfrastructure.Extensions;
using SuperShopInfrastructure.Static;

namespace SuperShopApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TestController : ControllerBase
    {
        //[HttpPost("")]
        //[ModelValidate]
        //public async Task<IActionResult> Test([FromBody] TestCommand command)
        //{
        //    var decErrMsg = command.DecryptProperty();
        //    if (!string.IsNullOrEmpty(decErrMsg))
        //        return BadRequest(decErrMsg);
        //    return Ok();
        //}

        [HttpGet("encrypt/obj")]
        public async Task<IActionResult> Encrypt([FromQuery] string str)
        {
            return Ok(SecretClass.EncryptQueryString(str));
        }
    }
}