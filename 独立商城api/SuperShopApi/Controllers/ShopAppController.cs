﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuperShopInfrastructure.Extensions;
using SuperShopInfrastructure.Models.ShopApp;
using SuperShopInfrastructure.Static;
using SuperShopService.IServices;

namespace SuperShopApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ShopAppController : ControllerBase
    {
        private readonly IShopAppService shopAppService;

        public ShopAppController(IShopAppService shopAppService)
        {
            this.shopAppService = shopAppService;
        }

        /// <summary>
        /// 获取店铺信息
        /// </summary>
        /// <param name="spid"></param>
        /// <returns></returns>
        [HttpGet("{spid}")]
        [ProducesResponseType(typeof(ShopAppViewModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetShopApp([FromRoute] string spid)
        {
            var _spid = ToolManager.DecryptParms(spid);
            if (_spid == null)
                return BadRequest("非法参数");
            var shopAppModel = await shopAppService.GetShopAppModel(_spid.ToInt());
            if (shopAppModel == null)
                return BadRequest("获取店铺模型出错");
            shopAppModel.AppId = "";
            shopAppModel.AppSecret = "";
            return Ok(shopAppModel);
        }
    }
}