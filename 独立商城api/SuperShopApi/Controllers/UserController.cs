﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CqBuiltinService.Controller;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuperShopInfrastructure.Extensions;
using SuperShopInfrastructure.Models.Question;
using SuperShopInfrastructure.Models.User;
using SuperShopInfrastructure.Static;
using SuperShopService.IServices;

namespace SuperShopApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService userService;
        private readonly IQuestionService questionService;

        public UserController(
            IUserService userService,
            IQuestionService questionService)
        {
            this.userService = userService;
            this.questionService = questionService;
        }
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [HttpPost("login/app")]
        [ProducesResponseType(typeof(WxUserLoginViewModel), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Login([FromBody] WxUserLoginCommand command)
        {
            var _spid = ToolManager.DecryptParms(command.Spid);
            if (_spid == null)
                return BadRequest("非法参数");
            command.Spid = _spid;
            var res = await userService.WxUserLoginOrRegister(command);
            if (!res.IsSuccess)
                return BadRequest(res.FailureReason);
            return Ok(res.GetData());
        }

        /// <summary>
        /// 投诉问题
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [ModelValidate]
        [HttpPost("question")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> AddQuestion(CreateQuestionCommand command)
        {
            var decErrMsg = command.DecryptProperty();
            if (!string.IsNullOrEmpty(decErrMsg))
                return BadRequest(decErrMsg);
            await questionService.Insert(command);
            return Ok(true);
        }
    }
}