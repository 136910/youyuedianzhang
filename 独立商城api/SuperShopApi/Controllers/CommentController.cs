﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CqBuiltinService.Controller;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuperShopInfrastructure.Extensions;
using SuperShopInfrastructure.Models.Comment;
using SuperShopInfrastructure.Static;
using SuperShopService.IServices;

namespace SuperShopApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly ICommentService commentService;

        public CommentController(ICommentService commentService)
        {
            this.commentService = commentService;
        }

        /// <summary>
        /// 用户创建订单评论
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [ModelValidate]
        [HttpPost("")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Create([FromBody] CreateCommentCommand command)
        {
            var decErrMsg = command.DecryptProperty();
            if (!string.IsNullOrEmpty(decErrMsg))
                return BadRequest(decErrMsg);
            var result = await commentService.Create(command);
            if (!result.IsSuccess)
                return BadRequest(result.FailureReason);
            return Ok(result.GetData());
        }

        /// <summary>
        /// 分页获取商品评论
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [ModelValidate]
        [HttpPost("search/data")]
        [ProducesResponseType(typeof(List<CommentViewModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> QueryComments([FromBody] QueryCommentCriteria command)
        {
            var decErrMsg = command.DecryptProperty();
            if (!string.IsNullOrEmpty(decErrMsg))
                return BadRequest(decErrMsg);
            var result = await commentService.QueryComments(command);
            return Ok(result);
        }

        /// <summary>
        /// 获取商品得评论数量
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpGet("count")]
        [ProducesResponseType(typeof(int), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> QueryComments([FromQuery] string productId)
        {
            string _pid = ToolManager.DecryptParms(productId);
            if (string.IsNullOrEmpty(_pid))
                return BadRequest("非法参数");
            var result = await commentService.GetCounts(_pid.ToInt());
            return Ok(result);
        }

       
    }
}