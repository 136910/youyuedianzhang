﻿using CqCore.Logging;
using Microsoft.Extensions.Options;
using SuperShopInfrastructure.Models.Order;
using SuperShopInfrastructure.Models.ShopCart;
using SuperShopInfrastructure.Static;
using SuperShopRepository;
using SuperShopService.IServices;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using UploadService.Option;

namespace SuperShopService.Services
{
    public class ShopCartService : IShopCartService
    {
        private readonly ShopCartRepository shopCartRepository;
        private readonly ILogRecorder<ShopCartService> logRecorder;
        private readonly ImageServiceOption imageServiceOption;

        public ShopCartService(
            ShopCartRepository shopCartRepository,
            ILogRecorder<ShopCartService> logRecorder,
            IOptions<ImageServiceOption> options)
        {
            this.shopCartRepository = shopCartRepository;
            this.logRecorder = logRecorder;
            this.imageServiceOption = options.Value;
        }

        public async Task<bool> CreateOrderInsert(CreateOrderCommand command)
        {
            return await shopCartRepository.CreateOrderInsert(command);
        }

        public async Task<List<ShopCartListViewModel>> GetShopCartList(string skuids)
        {
            //验证
            if (ToolManager.IsSqlin(skuids))
            {
                logRecorder.Error(new EventData()
                {
                    Type = "ShopCartService.GetShopCartList",
                    Message = "发生sql注入",
                    Labels = {
                            ["parms"] = skuids
                        }
                });
                return null;
            }
            var queryList = new List<ShopCartListViewModel>();
            var ds = await shopCartRepository.GetShopCartList(skuids);
            if (DataManager.CheckDs(ds, 1) && DataManager.CheckHasRow(ds.Tables[0]))
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    queryList.Add(new ShopCartListViewModel()
                    {
                        ProductId = SecretClass.EncryptQueryString(dr["ProductId"].ToString()),
                        Product_SkuId = Convert.ToInt32(dr["Product_SkuId"]),
                        SkuType = Convert.ToInt32(dr["SkuType"]),
                        StockNum = Convert.ToInt32(dr["StockNum"]),
                        ImgPath = imageServiceOption.ImgHost + dr["ImgPath"].ToString(),
                        ProductName = dr["ProductName"].ToString(),
                        Brand = dr["Brand"].ToString(),
                        ProppetyCombineName = dr["ProppetyCombineName"].ToString(),
                        SallPrice = DataManager.GetRowValue_Decimal(dr["SallPrice"]),
                        IsDel = Convert.ToInt32(dr["IsDel"]),
                        ShippingTemplateId = Convert.ToInt32(dr["ShippingTemplateId"]),
                        Weight = DataManager.GetRowValue_Float(dr["Weight"])
                    });
                }
            }
            return queryList;
        }
    }
}
