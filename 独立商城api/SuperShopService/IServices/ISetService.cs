﻿
using SuperShopInfrastructure.Models.Ship;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopService.IServices
{
    public interface ISetService
    {
        Task<ShippingTemplateModel> GetShippingTemplateModel(int id);
    }
}
