﻿using CqCore.Commands;
using SuperShopInfrastructure.Models.Consignee;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopService.IServices
{
    public interface IConsigneeService
    {
        Task<List<ConsigneeViewModel>> GetList(int userid);
        Task UpdateIsDefault(int id, int userid, int isdefault);
        Task Del(int id, int userid);
        Task Edit(CreateOrEditConsigneeCommand command);
        Task<CommandResult<bool>> Add(CreateOrEditConsigneeCommand command);
    }
}
