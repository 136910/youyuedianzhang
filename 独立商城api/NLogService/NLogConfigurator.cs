﻿using NLog;
using NLog.Common;
using NLog.Config;
using NLogService.Options;

namespace NLogService
{
    public class NLogConfigurator
    {
        public static void Config(NLogOption option)
        {
            InternalLogger.LogFile = $"{option.DataPath}/nlog.log";

            LogManager.Configuration = new XmlLoggingConfiguration(option.ConfigPath);
            LogManager.Configuration.Variables.Add("root", option.DataPath);
            LogManager.Configuration.Variables.Add("module", option.ModuleName);
            LogManager.Configuration.Variables.Add("node", option.NodeName);
        }
    }
}
