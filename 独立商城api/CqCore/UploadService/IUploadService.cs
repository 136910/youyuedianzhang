﻿using CqCore.UploadService.Base;
using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CqCore.UploadService
{
    public interface IUploadService
    {
        /// <summary>
        /// 站点保存到图片虚拟目录
        /// </summary>
        /// <param name="file"></param>
        /// <param name="uploader"></param>
        /// <returns></returns>
        Task<string> DomianUpload(IFormFile file, IFileUploader uploader);
        /// <summary>
        /// 批量上传到图片服务器
        /// </summary>
        /// <param name="imgs"></param>
        /// <param name="uploader"></param>
        /// <returns></returns>
        Task<bool> Upload(List<string> imgs, IFileUploader uploader);
        /// <summary>
        /// 单个上传到图片服务器
        /// </summary>
        /// <param name="img"></param>
        /// <param name="uploader"></param>
        /// <returns></returns>
        Task<bool> Upload(string img, IFileUploader uploader);
    }
}
