﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CqCore.Extensions
{
    public static class JsonExtensions
    {
        public static readonly JsonSerializerSettings DefaultSettings = new JsonSerializerSettings()
        {
            Formatting = Formatting.None,
            DateFormatHandling = DateFormatHandling.IsoDateFormat
        };

        public static JsonSerializerSettings Default(this JsonSerializerSettings context)
        {
            return DefaultSettings;
        }

        public static void MergeFrom(this JObject context, JObject content)
        {
            foreach (var item in content)
            {
                context[item.Key] = JToken.FromObject(item.Value);
            }
        }
    }
}
