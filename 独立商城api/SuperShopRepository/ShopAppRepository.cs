﻿using CqCore.DB;
using SuperShopRepository.Dbs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopRepository
{
    public class ShopAppRepository
    {
        private readonly ISqlService<SuperShopCustomerDb> sqlService;

        public ShopAppRepository(ISqlService<SuperShopCustomerDb> sqlService)
        {
            this.sqlService = sqlService;
        }

        public async Task<DataSet> GetShopAppModel(int id)
        {
            SqlParameter[] parms =
            {
                new SqlParameter("@id",id)
            };
            var sql = "select s.* from C_ShopApp s left join C_ShopAdmin cs on s.ShopAdminId=cs.Id  where s.Id = @id";
            return await sqlService.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public async Task<DataSet> GetPaykeyByAppid(string appid)
        {
            SqlParameter[] parms =
            {
                new SqlParameter("@appid",appid)
            };
            var sql = "select PaySecret from C_ShopApp where AppId=@appid";
            return await sqlService.ExecuteDataset(CommandType.Text, sql, parms);
        }
    }
}
