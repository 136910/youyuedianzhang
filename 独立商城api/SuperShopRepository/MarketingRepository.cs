﻿using CqCore.DB;
using SuperShopRepository.Dbs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopRepository
{
    public class MarketingRepository
    {
        private readonly ISqlService<SuperShopCustomerDb> sqlService;

        public MarketingRepository(ISqlService<SuperShopCustomerDb> sqlService)
        {
            this.sqlService = sqlService;
        }

        public async Task<DataSet> GetSetModel(int shopAdminId)
        {
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAdminId",shopAdminId)
            };
            var sql = "select * from C_FullAmountSet where ShopAdminId=@shopAdminId;";
            sql += " DECLARE @pidlist varchar(500) ";
            sql += " declare @areaList varchar(500) ";
            sql += " select @pidlist= NotHasProductIds,@areaList= NotHasProvinceIds from C_FullAmountSet where ShopAdminId=@shopAdminId ";
            sql += " EXEC('select ImagePath from C_ProductImg where ProductId in('+@pidlist+') and IsDefault=1 and IsDel=0') ";
            sql += " EXEC('select Area_Name from C_Areas where Area_Code in('+@areaList+')') ";
            return await sqlService.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public async Task<DataSet> GetUserCouponList(int userid)
        {
            SqlParameter[] parms =
           {
                new SqlParameter("@userid",userid)
            };
            var sql = "select CouponId,c.CouponName,c.CouponType, c.DelAmount,c.Discount,c.MinOrderAmount,uc.CreateTime,c.Days from C_UserCoupon uc left join C_Coupon c on uc.CouponId = c.Id where uc.IsUsed = 0 and uc.UserId = @userid ";
            return await sqlService.ExecuteDataset(CommandType.Text, sql, parms);
        }
        public async Task<DataSet> GetUserCoupon(int userid, int couponid)
        {
            SqlParameter[] parms =
           {
                new SqlParameter("@userid",userid),
                new SqlParameter("@couponid",couponid)
            };
            var sql = "select CouponId,c.CouponName,c.CouponType, c.DelAmount,c.Discount,c.MinOrderAmount,uc.CreateTime,c.Days from C_UserCoupon uc left join C_Coupon c on uc.CouponId = c.Id where uc.IsUsed = 0 and uc.UserId = @userid and uc.CouponId=@couponid";
            return await sqlService.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public async Task<DataSet> GetCouponList(int shopAdminId)
        {
            SqlParameter[] parms =
           {
                new SqlParameter("@shopAdminId",shopAdminId)
            };
            var sql = "select * from C_Coupon where ShopAdminId=@shopAdminId and IsDel=0";
            return await sqlService.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public async Task<int> GetCoupon(int userid, int couponid)
        {
            SqlParameter rtn_err = await sqlService.GetRtnParameter();
            SqlParameter[] parms =
            {
                new SqlParameter("@couponId", couponid),
                new SqlParameter("@userId",userid),
                rtn_err
            };
            await sqlService.ExecuteNonQuery(CommandType.StoredProcedure, "getcoupon", parms);
            if (rtn_err.Value != null)
            {
                return int.Parse(rtn_err.Value.ToString());
            }
            return -1;
        }
    }
}
