﻿using CqCore.Logging;
using CqCore.SMS;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using SMS.Options;
using SMS.Services;

namespace CqBuiltinService.SMS
{
    public static class SMSExtensions
    {
        public static IServiceCollection AddSMS(this IServiceCollection services)
        {
            var provider = services.BuildServiceProvider();
            var logger = provider.GetRequiredService<ILogRecorder<SMSService>>();
            var option = provider.GetRequiredService<IOptions<SmsOption>>().Value;
            var smsService = new SMSService(logger, option);
            services.AddSingleton<ISMSService>(smsService);
            return services;
        }
    }
}
