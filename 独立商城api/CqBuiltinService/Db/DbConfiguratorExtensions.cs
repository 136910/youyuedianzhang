﻿using CqCore.DB;
using DbService;
using Microsoft.Extensions.DependencyInjection;

namespace CqBuiltinService.Db
{
    /// <summary>
    /// 支持多个数据库，每个数据库一个链接字符串。如果是分库分表，不支持，应采用算法服务，计算出链接，在仓储曾，返回sqlservice对象
    /// </summary>
    public static class DbConfiguratorExtensions
    {
        public static IServiceCollection AddDb<TDb>(this IServiceCollection services, string constr) where TDb:IBaseDb
        {
            //
            services.AddSingleton<ISqlService<TDb>>(p=> {
                return new SqlService<TDb>(constr);
            });
            return services;
        }
    }
}
