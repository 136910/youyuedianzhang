﻿using System;

namespace CqBuiltinService.Environments
{
    public class Variables
    {
        public static string NODE_NAME => Environment.GetEnvironmentVariable("NODE_NAME");
        public static string CONSUL_HOST => Environment.GetEnvironmentVariable("CONSUL_HOST");
        public static string CONSUL_TOKEN => Environment.GetEnvironmentVariable("CONSUL_TOKEN");
        public static string CONSUL_KEY => Environment.GetEnvironmentVariable("CONSUL_KEY");
        public static string COMPUTERNAME => Environment.GetEnvironmentVariable("COMPUTERNAME");
    }
}
