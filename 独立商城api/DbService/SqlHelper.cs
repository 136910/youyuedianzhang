﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace DbService
{
    public abstract class SqlHelper
    {

        //Database connection strings
        //public static readonly string ConnectionStringLocalTransaction = ConfigurationManager.ConnectionStrings["SQLConnString1"].ConnectionString;
        //public static readonly string ConnectionStringInventoryDistributedTransaction = ConfigurationManager.ConnectionStrings["SQLConnString2"].ConnectionString;
        //public static readonly string ConnectionStringOrderDistributedTransaction = ConfigurationManager.ConnectionStrings["SQLConnString3"].ConnectionString;
        //public static readonly string ConnectionStringProfile = ConfigurationManager.ConnectionStrings["SQLProfileConnString"].ConnectionString;

        // Hashtable to store cached parameters
        private static Hashtable parmCache = Hashtable.Synchronized(new Hashtable());

        /// <summary>
        /// Execute a SqlCommand (that returns no resultset) against the database specified in the connection string 
        /// using the provided parameters.
        /// </summary>
        /// <remarks>
        /// e.g.:  
        ///  int result = ExecuteNonQuery(connString, CommandType.StoredProcedure, "PublishOrders", new SqlParameter("@prodid", 24));
        /// </remarks>
        /// <param name="connectionString">a valid connection string for a SqlConnection</param>
        /// <param name="commandType">the CommandType (stored procedure, text, etc.)</param>
        /// <param name="commandText">the stored procedure name or T-SQL command</param>
        /// <param name="commandParameters">an array of SqlParamters used to execute the command</param>
        /// <returns>an int representing the number of rows affected by the command</returns>
        public async Task<int> ExecuteNonQuery(string connectionString, CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
        {
            SqlCommand cmd = new SqlCommand();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                PrepareCommand(cmd, conn, null, cmdType, cmdText, commandParameters);
                int val = await cmd.ExecuteNonQueryAsync();
                cmd.Parameters.Clear();
                return val;
            }
        }

        public async Task<DataSet> ExecuteDataset(string connectionString, CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
        {
            SqlCommand cmd = new SqlCommand();
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlDataAdapter pp = new SqlDataAdapter();
                await PrepareCommand(cmd, conn, null, cmdType, cmdText, commandParameters);
                pp.SelectCommand = cmd;
                DataSet ds = new DataSet();
                pp.Fill(ds);
                return ds;
            }
        }


        /// <summary>
        /// Execute a SqlCommand that returns a resultset against the database specified in the connection string 
        /// using the provided parameters.
        /// </summary>
        /// <remarks>
        /// e.g.:  
        ///  SqlDataReader r = ExecuteReader(connString, CommandType.StoredProcedure, "PublishOrders", new SqlParameter("@prodid", 24));
        /// </remarks>
        /// <param name="connectionString">a valid connection string for a SqlConnection</param>
        /// <param name="commandType">the CommandType (stored procedure, text, etc.)</param>
        /// <param name="commandText">the stored procedure name or T-SQL command</param>
        /// <param name="commandParameters">an array of SqlParamters used to execute the command</param>
        /// <returns>A SqlDataReader containing the results</returns>
        public async Task<SqlDataReader> ExecuteReader(string connectionString, CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
        {
            SqlCommand cmd = new SqlCommand();
            SqlConnection conn = new SqlConnection(connectionString);

            // we use a try/catch here because if the method throws an exception we want to 
            // close the connection throw code, because no datareader will exist, hence the 
            // commandBehaviour.CloseConnection will not work
            try
            {
               await PrepareCommand(cmd, conn, null, cmdType, cmdText, commandParameters);
                SqlDataReader rdr = await cmd.ExecuteReaderAsync(CommandBehavior.CloseConnection);
                cmd.Parameters.Clear();
                conn.Close();
                return rdr;
            }
            catch
            {
                conn.Close();
                throw;
            }
        }

        /// <summary>
        /// Execute a SqlCommand that returns the first column of the first record against the database specified in the connection string 
        /// using the provided parameters.
        /// </summary>
        /// <remarks>
        /// e.g.:  
        ///  Object obj = ExecuteScalar(connString, CommandType.StoredProcedure, "PublishOrders", new SqlParameter("@prodid", 24));
        /// </remarks>
        /// <param name="connectionString">a valid connection string for a SqlConnection</param>
        /// <param name="commandType">the CommandType (stored procedure, text, etc.)</param>
        /// <param name="commandText">the stored procedure name or T-SQL command</param>
        /// <param name="commandParameters">an array of SqlParamters used to execute the command</param>
        /// <returns>An object that should be converted to the expected type using Convert.To{Type}</returns>
        public async Task<object> ExecuteScalar(string connectionString, CommandType cmdType, string cmdText, params SqlParameter[] commandParameters)
        {
            SqlCommand cmd = new SqlCommand();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                PrepareCommand(cmd, connection, null, cmdType, cmdText, commandParameters);
                object val = await cmd.ExecuteScalarAsync();
                cmd.Parameters.Clear();
                return val;
            }
        }


        /// <summary>
        /// add parameter array to the cache
        /// </summary>
        /// <param name="cacheKey">Key to the parameter cache</param>
        /// <param name="cmdParms">an array of SqlParamters to be cached</param>
        public static void CacheParameters(string cacheKey, params SqlParameter[] commandParameters)
        {
            parmCache[cacheKey] = commandParameters;
        }

        /// <summary>
        /// Retrieve cached parameters
        /// </summary>
        /// <param name="cacheKey">key used to lookup parameters</param>
        /// <returns>Cached SqlParamters array</returns>
        public static SqlParameter[] GetCachedParameters(string cacheKey)
        {
            SqlParameter[] cachedParms = (SqlParameter[])parmCache[cacheKey];

            if (cachedParms == null)
                return null;

            SqlParameter[] clonedParms = new SqlParameter[cachedParms.Length];

            for (int i = 0, j = cachedParms.Length; i < j; i++)
                clonedParms[i] = (SqlParameter)((ICloneable)cachedParms[i]).Clone();

            return clonedParms;
        }

        /// <summary>
        /// Prepare a command for execution
        /// </summary>
        /// <param name="cmd">SqlCommand object</param>
        /// <param name="conn">SqlConnection object</param>
        /// <param name="trans">SqlTransaction object</param>
        /// <param name="cmdType">Cmd type e.g. stored procedure or text</param>
        /// <param name="cmdText">Command text, e.g. Select * from Products</param>
        /// <param name="cmdParms">SqlParameters to use in the command</param>
        private async Task PrepareCommand(SqlCommand cmd, SqlConnection conn, SqlTransaction trans, CommandType cmdType, string cmdText, SqlParameter[] cmdParms)
        {

            if (conn.State != ConnectionState.Open)
                await conn.OpenAsync();

            cmd.Connection = conn;
            cmd.CommandText = cmdText;

            if (trans != null)
                cmd.Transaction = trans;

            cmd.CommandType = cmdType;

            if (cmdParms != null)
            {
                foreach (SqlParameter parm in cmdParms)
                    cmd.Parameters.Add(parm);
            }
        }

        /// <summary>
        /// 封装ado.net的事务的更新操作，不能跨越多个数据库连接
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="paramers"></param>
        /// <returns></returns>
        public async Task<bool> ExecTransactionAsUpdate(string[] sql, string connectionStr, List<SqlParameter[]> paramers = null)
        {
            var result = true;
            int[] arr = new int[sql.Length];
            int rowCount = 0;//影响的行数
            var SqlConn = new SqlConnection(connectionStr);
            SqlConn.Open();
            SqlTransaction sqlTransaction = SqlConn.BeginTransaction();
            try
            {
                var sqlCommand = new SqlCommand { Transaction = sqlTransaction, Connection = SqlConn, CommandType = CommandType.Text };
                for (int i = 0; i < sql.Length; i++)
                {
                    sqlCommand.CommandText = sql[i];
                    if (paramers != null)
                    {
                        sqlCommand.Parameters.AddRange(paramers[i]);
                    }
                    arr[i] = await sqlCommand.ExecuteNonQueryAsync();
                }
                sqlTransaction.Commit();

                result = true;

            }
            catch (Exception e)
            {
                sqlTransaction.Rollback();
                result = false;
            }
            finally
            {
                SqlConn.Close();
            }

            return result;
        }

        /// <summary>
        /// 封装ado.net的事务的更新操作，不能跨越多个数据库连接
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="paramers"></param>
        /// <returns></returns>
        public async Task<bool> ExecTransactionAsUpdate_Public(string[] sql, string connectionStr, SqlParameter[] paramers = null)
        {
            var result = true;
            int[] arr = new int[sql.Length];
            int rowCount = 0;//影响的行数
            var SqlConn = new SqlConnection(connectionStr);
            SqlConn.Open();
            SqlTransaction sqlTransaction = SqlConn.BeginTransaction();
            try
            {
                var sqlCommand = new SqlCommand { Transaction = sqlTransaction, Connection = SqlConn, CommandType = CommandType.Text };
                if (paramers != null)
                {
                    sqlCommand.Parameters.AddRange(paramers);
                }
                for (int i = 0; i < sql.Length; i++)
                {
                    sqlCommand.CommandText = sql[i];

                    arr[i] = await sqlCommand.ExecuteNonQueryAsync();
                }
                sqlTransaction.Commit();

                result = true;

            }
            catch (Exception e)
            {
                sqlTransaction.Rollback();
                result = false;
            }
            finally
            {
                SqlConn.Close();
            }

            return result;
        }
    }
}
