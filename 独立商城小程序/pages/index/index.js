const app = getApp();
//app.initConfig(app);
Page({

  /**
   * 页面的初始数据
   */
  data: {
    mallIndexData: null,
    menus: null,
    specialPlace: null,
    specialArea:null,
    words:null,
    keyword:null
  },
  jumpMenu:function(e){
    var menuid = e.currentTarget.dataset.menuid;
    app.globalData.menuid=menuid;
    wx.switchTab({
      url: '/pages/menu/menulist',
    })
  },
  spanSearch:function(e){
    var keyword = e.currentTarget.dataset.hot;
    app.globalData.keyword = keyword;
    wx.switchTab({
      url: '/pages/menu/menulist',
    })
  },
  keyWordChange: function (ev) {
    this.setData({
      keyword: ev.detail.value
    });
  },
  btnSearch: function () {
    app.globalData.keyword = this.data.keyword;
    wx.switchTab({
      url: '/pages/menu/menulist',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    //wx.hideShareMenu();
    var that = this;
   
    app.initConfig().then(function(res){
      if(res==200){
        wx.setNavigationBarTitle({
          title: app.appSetting.name  //修改title
        })
      }
      
    })
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {
    var that = this;
    app.initConfig().then(function (res) {
      if (res == 200) {
        that.getIndexData();
        that.getMenus();
        that.getSpecialPlace();
        that.getSpecialArea();
        that.getHotWords();
      }

    })
    
  },
  getHotWords:function(){
    var that = this;
    wx.request({
      url: app.appSetting.host + "/SearchKeyword?said=" + app.appSetting.said,
      data: {},
      method:'GET',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function(res) {
        console.log(res);
        if (res.statusCode == 200) {
          if (res.data.length >= 0) {
            that.setData({
              words: res.data
            })
          }
        } else {
          wx.showToast({
            title: res.data,
            icon: 'none'
          })
        }
      }
    })
  },

  getMenus: function() {
    var that = this;
    wx.request({
      url: app.appSetting.host + "/product/menuList?said=" + app.appSetting.said,
      data: {},
      method:'GET',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function(res) {
        console.log(res);
        if (res.statusCode == 200) {
          if (res.data.length >= 0) {
            that.setData({
              menus: res.data
            })
          }
        } else {
          wx.showToast({
            title: res.data,
            icon: 'none'
          })
        }
      }
    })
  },
  getSpecialArea: function() {
    var that = this;
    wx.request({
      url: app.appSetting.host + "/Product/specialPlaces?said=" + app.appSetting.said+"&type=2",
      data: {},
      method:"GET",
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function(res) {
        if (res.statusCode==200) {
          if (res.data.length > 0) {
            that.setData({
              specialArea: res.data
            })
          }
        } else {
          wx.showToast({
            title: res.data,
            icon: 'none'
          })
        }
      }
    })
  },
  getSpecialPlace: function() {
    var that = this;
    wx.request({
      url: app.appSetting.host + "/Product/specialPlaces?said=" + app.appSetting.said+"&type=1",
      data: {},
      method:"GET",
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function(res) {
        if (res.statusCode==200) {
          if (res.data.length > 0) {
            that.setData({
              specialPlace: res.data
            })
          }
        } else {
          wx.showToast({
            title: res.data,
            icon: 'none'
          })
        }
      }
    })
  },
  getIndexData: function() {
    var that = this;
    wx.showLoading({
      title: '努力加载中...',
    })
    wx.request({
      //url: app.appSetting.host + "/api/MobileMall/index?ShopID=1AD227F46DFB1666",
      url: app.appSetting.host + "/Home/data?spid=" + app.appSetting.spid + "&said=" + app.appSetting.said,
      data: {},
      method:"GET",
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function(res) {
        console.log(res);
        wx.hideLoading();
        if (res.statusCode==200) {
          that.setData({
            mallIndexData: res.data,
          })
        } else {
          wx.showToast({
            title: res.data,
            icon: 'none'
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    TestData.MobileMallIndex().then((res) => {
      app.ISDEBUGGER && console.log(res);
      this.setData({
        mallIndexData: res.Data,
      })
      wx.stopPullDownRefresh()
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  goToGoodsList() {
    wx.navigateTo({
      url: "/pages/menu/menu?menuid=0"
    });
  }
})