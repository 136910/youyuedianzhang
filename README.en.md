# 优越店长.net微商城Sass平台版本及.net core独立商户部署版本

#### Description
优越店长是一款基于.NET4.6开发的一款完整的微信商城SAAS平台，前端支持小程序、h5，由前端商城，商户管理后台，平台管理后台三大块组成，sass功能完善，支持商户拖拽式零代码创建并提交上传发布小程序，商城功能模块完善，用户管理，商品管理(支持多规格)，订单管理，运费模板管理，用户管理，营销管理等等。与此同时，也提供基于.net core2.1开发的独立部署版本商城小程序端+管理后台。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
