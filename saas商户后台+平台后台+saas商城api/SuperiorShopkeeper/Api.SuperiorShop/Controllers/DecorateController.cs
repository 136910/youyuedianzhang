﻿using Redis;
using SuperiorModel;
using SuperiorShopBussinessService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Api.SuperiorShop.Controllers
{
    [ApiExceptionAttribute]
    public class DecorateController : ApiControllerBase
    {
        private readonly IDecorateService decorateService;

        public DecorateController(IDecorateService decorateService)
        {
            this.decorateService = decorateService;

        }
        protected override void Dispose(bool disposing)
        {
            this.decorateService.Dispose();
            base.Dispose(disposing);
        }

        /// <summary>
        /// 获取首页装修设计
        /// </summary>
        /// <param name="spid"></param>
        /// <param name="said"></param>
        /// <returns></returns>
        [HttpGet]
        public ApiResultModel<object> GetDecorateDesign(string spid, string said)
        {
            var _spid = base.ValidataParms(spid);
            if (_spid == null)
                return Error("非法参数");
            var _said = base.ValidataParms(said);
            if (_said == null)
                return Error("非法参数");
            Func<DecorateModel> fucc = () =>
            {
                return decorateService.GetDecorateModel(_said.ToInt());
            };
            var res = DataIntegration.GetData<DecorateModel>("GetDecorateModel_" + _said, fucc, 24);
            return Success(res);
        }

    }
}
