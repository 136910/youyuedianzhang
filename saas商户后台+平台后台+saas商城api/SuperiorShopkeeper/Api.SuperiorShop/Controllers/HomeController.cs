﻿using SuperiorModel;
using SuperiorShopBussinessService;
using System;
using System.Web.Http;
using Redis;
using System.Collections.Generic;
using System.Linq;

namespace Api.SuperiorShop.Controllers
{
    [ApiExceptionAttribute]
    public class HomeController : ApiControllerBase
    {
        // GET: Home
        private readonly IProductService _IProductService;
        private readonly IShopAppService _IShopAppService;
        private readonly ISearchWordService _ISearchWordService;

        public HomeController(IProductService IProductService, IShopAppService IShopAppService, ISearchWordService ISearchWordService)
        {
            _IProductService = IProductService;
            _IShopAppService = IShopAppService;
            _ISearchWordService = ISearchWordService;

        }
        protected override void Dispose(bool disposing)
        {
            this._IProductService.Dispose();
            this._IShopAppService.Dispose();
            this._ISearchWordService.Dispose();
            base.Dispose(disposing);
        }
        /// <summary>
        ///获取商城首页数据
        /// </summary>
        /// <param name="spid"></param>
        /// <returns></returns> 
        [HttpGet]
        public ApiResultModel<object> GetHomeData(string spid, string said)
        {
            var _spid = base.ValidataParms(spid);
            if (_spid == null)
                return Error("非法参数");
            var _said = base.ValidataParms(said);
            if (_said == null)
                return Error("非法参数");
            Func<Api_HomeDataModel> fucc = delegate ()
            {
                return _IProductService.Api_GetHomeData(_spid.ToInt());
            };
            var res = DataIntegration.GetData<Api_HomeDataModel>("GetHomeData_" + _said, fucc, 1);
            return Success(res);
        }

        /// <summary>
        /// 获取轮播图数据
        /// </summary>
        /// <param name="spid"></param>
        /// <param name="said"></param>
        /// <returns></returns>
        [HttpGet]
        public ApiResultModel<object> GetBanners(string spid, string said)
        {
            var _spid = base.ValidataParms(spid);
            if (_spid == null)
                return Error("非法参数");
            var _said = base.ValidataParms(said);
            if (_said == null)
                return Error("非法参数");
            Func<List<Api_ProductItem>> fucc = delegate ()
            {
                return _IProductService.Api_GetBanners(_said.ToInt());
            };
            var res = DataIntegration.GetData<List<Api_ProductItem>>("GetBanners_" + _said, fucc, 1);
            return Success(res);
        }

        /// <summary>
        /// 获取热门商品数据
        /// </summary>
        /// <param name="spid"></param>
        /// <param name="said"></param>
        /// <returns></returns>
        [HttpGet]
        public ApiResultModel<object> GetHotProducts(string spid, string said)
        {
            var _spid = base.ValidataParms(spid);
            if (_spid == null)
                return Error("非法参数");
            var _said = base.ValidataParms(said);
            if (_said == null)
                return Error("非法参数");
            Func<List<Api_ProductItem>> fucc = delegate ()
            {
                return _IProductService.Api_GetHotProducts(_said.ToInt());
            };
            var res = DataIntegration.GetData<List<Api_ProductItem>>("GetHotProducts_" + _said, fucc, 1);
            return Success(res);
        }

        /// <summary>
        /// 获取推荐商品数据
        /// </summary>
        /// <param name="spid"></param>
        /// <param name="said"></param>
        /// <returns></returns>
        [HttpGet]
        public ApiResultModel<object> GetCommandProducts(string spid, string said)
        {
            var _spid = base.ValidataParms(spid);
            if (_spid == null)
                return Error("非法参数");
            var _said = base.ValidataParms(said);
            if (_said == null)
                return Error("非法参数");
            Func<List<Api_ProductItem>> fucc = delegate ()
            {
                return _IProductService.Api_GetCommandProducts(_said.ToInt());
            };
            var res = DataIntegration.GetData<List<Api_ProductItem>>("GetCommandProducts_" + _said, fucc, 1);
            return Success(res);
        }

        /// <summary>
        /// 获取搜索词
        /// </summary>
        /// <param name="said"></param>
        /// <returns></returns>
        [HttpGet]
        public ApiResultModel<object> GetSearchKeyword(string said)
        {
            var _said = base.ValidataParms(said);
            if (_said == null)
                return Error("非法参数");
            Func<List<SearchWordModel>> fucc = delegate ()
            {
                return _ISearchWordService.GetList(_said.ToInt());
            };
            var res = DataIntegration.GetData<List<SearchWordModel>>("GetSearchKeyword_" + _said, fucc, 24);
            res = res.Where(p => p.OptionStatus == 1).ToList();
            return Success(res);
        }
        /// <summary>
        /// 检测小程序或H5商城是否过期
        /// </summary>
        /// <param name="said"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public ApiResultModel<object> CheckShop(string spid, int apptype)
        {
            var _spid = base.ValidataParms(spid);
            if (_spid == null)
                return Error("检测非法");
            Func<ShopAppModel> fucc = delegate ()
            {
                return _IShopAppService.GetShopAppModel(_spid.ToInt());
            };
            var shopAppModel = DataIntegration.GetData<ShopAppModel>("GetShopAppModel_" + _spid, fucc);
            if (shopAppModel == null)
                return Error("获取店铺模型出错");
            if (string.IsNullOrEmpty(shopAppModel.ShopAdminId))
            {
                RedisManager.Remove("GetShopAppModel_" + _spid);
                return Error("获取店铺信息出错");
            }
            var res = true;
            if (apptype == 1)
            {
                res = shopAppModel.ProgrameIsExpire;
            }
            else if (apptype == 2)
            {
                res = shopAppModel.H5IsExpire;
            }
            return Success(res);
        }
    }
}