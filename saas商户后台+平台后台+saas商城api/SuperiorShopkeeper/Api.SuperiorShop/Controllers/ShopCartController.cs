﻿using SuperiorModel;
using SuperiorShopBussinessService;

namespace Api.SuperiorShop.Controllers
{
    /// <summary>
    /// 整个购物车不与数据库或者redis交互 ，以免数据量过大频繁的增删改锁表，每次加载购物车时，只根据用户保存在本地的购物车skuid，来查询最新的数据模型给客户端展示。
    /// </summary>
    [ApiExceptionAttribute]
    public class ShopCartController : ApiControllerBase 
    {
        private readonly IShopCartService _IShopCartService;

        public ShopCartController(IShopCartService IShopCartService)
        {
            _IShopCartService = IShopCartService;

        }
        protected override void Dispose(bool disposing)
        {
            this._IShopCartService.Dispose();
            base.Dispose(disposing);
        }
        /// <summary>
        /// 通过用户本地购物车存储的skuids比如1,2,3来换取最新的购物车所需商品模型的集合
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="token"></param>
        /// <param name="skuids"></param>
        /// <returns></returns>
        public ApiResultModel<object> GetShopCartModelList(string userid, string token, string skuids)
        {
            string _userid = base.ValidataParms(userid);
            if (_userid == null)
                return Error("非法用户参数");
            var token_key = "Token_" + userid;
            //if (!ValidataTool.CheckToken(token_key, token))
            //    return Error("用户token异常，请从新登录");
            var res = _IShopCartService.Api_GetShopCartList(skuids);
            if (res == null)
                return Error("非法参数");
            return Success(res);
        }

    }
}