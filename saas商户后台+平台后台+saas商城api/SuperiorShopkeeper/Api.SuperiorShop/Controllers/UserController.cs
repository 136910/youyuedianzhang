﻿using System;
using System.Web.Http;
using SuperiorModel;
using SuperiorCommon;
using SuperiorShopBussinessService;
using Redis;
using Newtonsoft.Json.Linq;

namespace Api.SuperiorShop
{
    [ApiException]
    public class UserController : ApiControllerBase
    {
        private readonly IUserService _IUserService;
        private readonly IShopAppService _IShopAppService;
        private readonly IQuestionService _IQuestionService;

        public UserController(IUserService IUserService, IShopAppService IShopAppService, IQuestionService IQuestionService)
        {
            _IUserService = IUserService;
            _IShopAppService = IShopAppService;
            _IQuestionService = IQuestionService;

        }
        protected override void Dispose(bool disposing)
        {
            this._IUserService.Dispose();
            this._IShopAppService.Dispose();
            this._IQuestionService.Dispose();
            base.Dispose(disposing);
        }

        //[Validate]
        [HttpGet]
        public ApiResultModel<object> Test()
        {
            //var res = ConfigManager.GetValue("WebHost");
            //var query = _ITestService.TestList();
            return ApiResultModel<object>.Conclude(ApiResultEnum.Success, "aaaaa");
        }
        /// <summary>
        /// 用户登录
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResultModel<object> Login(Api_WxUserLoginCriteria criteria)
        {
            var _spid = base.ValidataParms(criteria.Spid);
            if (_spid == null)
                return Error("非法参数");
            criteria.Spid = _spid;
            Func<ShopAppModel> fucc = delegate ()
            {
                return _IShopAppService.GetShopAppModel(_spid.ToInt());
            };
            var shopAppModel = DataIntegration.GetData<ShopAppModel>("GetShopAppModel_" + _spid, fucc);
            if (shopAppModel == null)
                return Error("获取店铺模型出错");
            //通过code获取token和openid
            string authorJson = WeiXinHelper.GetOpenIdStr(shopAppModel.AppId, shopAppModel.AppSecret, criteria.Code);
            LogManger.Instance.WriteLog("微信获取OPENID返回数据：" + authorJson);
            JObject jo = JObject.Parse(authorJson);
            string openid = jo["openid"].ToString();
            criteria.OpenId = openid;
            var res = _IUserService.Api_WxUserLoginOrRegister(criteria);
            if (string.IsNullOrEmpty(res.Token))
                return Error("用户请求异常");
            //存储token到redis,用于验证请求
            RedisManager.Set<string>("Token_" + res.Id, res.Token,DateTime.Now.AddDays(24));
            res.ShopName = shopAppModel.ShopName;
            return Success(res);
        }
        /// <summary>
        /// 获取店铺信心
        /// </summary>
        [HttpGet]
        public ApiResultModel<object> GetShopApp(string spid)
        {
            var _spid = base.ValidataParms(spid);
            if (_spid == null)
                return Error("非法参数");
            Func<ShopAppModel> fucc = delegate ()
            {
                return _IShopAppService.GetShopAppModel(_spid.ToInt());
            };
            var shopAppModel = DataIntegration.GetData<ShopAppModel>("GetShopAppModel_" + _spid, fucc);
            if (shopAppModel == null)
                return Error("获取店铺模型出错");
            shopAppModel.AppId = "";
            shopAppModel.AppSecret = "";
            return Success(shopAppModel);
        }
        /// <summary>
        /// 投诉问题
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResultModel<object> AddQuestion(Api_QuestionParms model)
        {
            var _spid = base.ValidataParms(model.ShopAppId);
            if (_spid == null)
                return Error("非法参数");
            var _said = base.ValidataParms(model.ShopAdminId);
            if (_said == null)
                return Error("非法参数");
            var _userid = base.ValidataParms(model.UserId);
            if (_userid == null)
                return Error("非法参数");
            var parms = new QuestionParms() {
                ShopAppId = _spid.ToInt(),
                ShopAdminId = _said.ToInt(),
                UserId = _userid.ToInt(),
                AppType = 1,
                Question = model.Question
            };
            _IQuestionService.Insert(parms);
            return Success(true);
        }
            
       
    }
}
