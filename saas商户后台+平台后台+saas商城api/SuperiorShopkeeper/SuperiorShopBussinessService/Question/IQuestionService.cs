﻿using SuperiorModel;

namespace SuperiorShopBussinessService
{
    public interface IQuestionService:IService
    {
        void Insert(QuestionParms model);
        PagedSqlList<QuestionModel> SearchQuestionList(QuestionCriteira criteria);
    }
}
