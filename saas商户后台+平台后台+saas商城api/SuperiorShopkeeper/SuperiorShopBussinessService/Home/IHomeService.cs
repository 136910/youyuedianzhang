﻿using SuperiorModel;

namespace SuperiorShopBussinessService
{
    public interface IHomeService:IService
    {
        HomeModel GetHomeData(int shopAdminId);
        AdminHomeModel GetAdminHomeData();
        Chart_AddUserModel GetAddUserChart(int shopAdminId);
        Chart_TypeUserModel GetTypeUserChart(int shopAdminId);
        Chart_SallModel GetSallChart(int shopAdminId);
        Chart_AddUserModel GetAdminAddUserChart();
        Chart_TypeUserModel GetAdminTypeUserChart();
        Chart_SallModel GetAdminSallChart();
        CreateRechargeOrderResult CreateRechargeOrder(int shopAdminId, int rechargeId, out string msg);
        int WxRechargeCall(string orderno, string payCode, int state, out string msg);
        int GetRechargeOrderStatus(int id);
    }
}
