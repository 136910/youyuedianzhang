﻿using System.Collections.Generic;
using SuperiorModel;

namespace SuperiorShopBussinessService
{
    public interface IShopService :IService
    {
        ShopInfoModel GetShopModel(string username, string password);
        ShopInfoModel GetShopModel_Role(string username, string password);
        int InsertShop(ShopInfoModel model,out string msg);
        List<RechargeModel> GetRechargeList();
        List<int> GetShopAppIdsByShopAdminId(int shopAdminId);
      

        void UpdateShopAdmin(string openid, string nickname, string headImgUrl, int id);
        SubmitCashOutResultModel ShopCashOutAmount(int shopAdminId, decimal amount,string orderno, out string msg);
        void UpdateShopCashCoutOder(string orderno, int status);
        PagedSqlList<ShopCashOutModel> SearchShopCashOutList(ShopCashOutCriteria criteria);
    }
}
