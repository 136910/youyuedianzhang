﻿using SuperiorModel;
using System.Collections.Generic;

namespace SuperiorShopBussinessService
{
    public interface IMarketingService:IService
    {
        //List<int> GetNotHasProductIds(int shopAdminId);
        //List<int> GetNotHasAreaIds(int shopAdminId);
        FullSetModel GetSetModel(int shopAdminId);
        void SetNotPids(string ids, int shopAdminId);
        void SetNotAreas(string ids, int shopAdminId);
        void SetFullAmount(int optionStatus, int amount, int shopAdminId);

        List<CouponModel> GetCouponList(int shopAdminId);
        void AddCoupon(CouponModel model);
        void DelCoupon(int id);
        PagedSqlList<UserCouponModel> SearchUserCoupon(UserCouponCriteria criteria);
        PagedSqlList<CouponModel> SearchAdminCoupon(AdminCouponCriteria criteria);


        #region 接口
        List<Api_UserCouponItem> Api_GetUserCouponList(int userid);
        Api_UserCouponItem Api_GetUserCoupon(int userid, int couponid);
        List<Api_CouponItem> Api_GetCouponList(int shopAdminId);
        int Api_GetCoupon(int userid, int couponid,out string msg);
        #endregion
    }
}
