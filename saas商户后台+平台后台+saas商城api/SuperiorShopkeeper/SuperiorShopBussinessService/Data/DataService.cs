﻿using System;
using System.Collections.Generic;
using SuperiorModel;
using SuperiorShopDataAccess;
using System.Data;


namespace SuperiorShopBussinessService
{
    public class DataService : IDataService
    {
        public void Dispose()
        {
            
        }

        public PagedSqlList<ShopOrderRankModel> SearchShopOrderRankDs(ShopOrderRankCriteria criteria)
        {
            var queryList = new List<ShopOrderRankModel>();
            var totalCount = 0;
            var ds = DataDataAccess.SearchShopOrderRankDs(criteria);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new ShopOrderRankModel()
                        {
                            ShopAdminId = Convert.ToInt32(dr["ShopAdminId"]),
                            LoginName = dr["LoginName"].ToString(),
                            TotalOrderCount =Convert.ToInt32(dr["TotalOrderCount"]),
                            ProgramOrderCount = Convert.ToInt32(dr["ProgramOrderCount"]),
                            H5OrderCount = Convert.ToInt32(dr["H5OrderCount"])
                        });
                    }
                }
                totalCount = Convert.ToInt32(ds.Tables[1].Rows[0]["totalCount"]);
            }
            return new PagedSqlList<ShopOrderRankModel>(queryList, criteria.PagingResult.PageIndex, criteria.PagingResult.PageSize, totalCount);
        }
       public  PagedSqlList<ShopUserRankModel> SearchShopUserRankDs(ShopUserRankCriteria criteria)
        {
            var queryList = new List<ShopUserRankModel>();
            var totalCount = 0;
            var ds = DataDataAccess.SearchShopUserRankDs(criteria);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new ShopUserRankModel()
                        {
                            ShopAdminId = Convert.ToInt32(dr["ShopAdminId"]),
                            LoginName = dr["LoginName"].ToString(),
                            TotalUserCount = Convert.ToInt32(dr["TotalUserCount"]),
                            ProgramUserCount = Convert.ToInt32(dr["ProgramUserCount"]),
                            H5UserCount = Convert.ToInt32(dr["H5UserCount"])
                        });
                    }
                }
                totalCount = Convert.ToInt32(ds.Tables[1].Rows[0]["totalCount"]);
            }
            return new PagedSqlList<ShopUserRankModel>(queryList, criteria.PagingResult.PageIndex, criteria.PagingResult.PageSize, totalCount);
        }
        public PagedSqlList<ShopAppExpressRankModel> SearchShopAppExpressRankDs(ShopAppExpressRankCriteria criteria)
        {
            var queryList = new List<ShopAppExpressRankModel>();
            var totalCount = 0;
            var ds = DataDataAccess.SearchShopAppExpressRankDs(criteria);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new ShopAppExpressRankModel()
                        {
                            Id=Convert.ToInt32(dr["Id"]),
                            ShopAdminId = Convert.ToInt32(dr["ShopAdminId"]),
                            LoginName = dr["LoginName"].ToString(),
                            AppType = Convert.ToInt32(dr["AppType"]),
                            ExpressTime = DataManager.GetRowValue_DateTime(dr["ExpressTime"]),
                            ShopName = dr["ShopName"].ToString()
                        });
                    }
                }
                totalCount = Convert.ToInt32(ds.Tables[1].Rows[0]["totalCount"]);
            }
            return new PagedSqlList<ShopAppExpressRankModel>(queryList, criteria.PagingResult.PageIndex, criteria.PagingResult.PageSize, totalCount);
        }
    }
}
