﻿using SuperiorCommon;
using SuperiorModel;
using SuperiorShopDataAccess;
using System;
using System.Collections.Generic;
using System.Data;

namespace SuperiorShopBussinessService
{
    public class UserService : IUserService
    {
        public PagedSqlList<UserListItem> Search(UserCriteria criteria)
        {
            var queryList = new List<UserListItem>();
            var totalCount = 0;
            var ds = UserDataAccess.Search(criteria);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new UserListItem()
                        {
                            Id = SecretClass.EncryptQueryString(dr["Id"].ToString()),
                            CreateTime =Convert.ToDateTime(dr["CreateTime"]),
                            ShopName = dr["ShopName"].ToString(),
                            AppType = Convert.ToInt32(dr["AppType"]),
                            NickName = dr["NickName"].ToString(),
                            OptionStatus = Convert.ToInt32(dr["OptionStatus"]),
                            LoginName = dr["LoginName"].ToString(),
                            HeadImgUrl = dr["HeadImgUrl"].ToString()
                           
                        });
                    }
                }
                totalCount = Convert.ToInt32(ds.Tables[1].Rows[0]["totalCount"]);
            }
            return new PagedSqlList<UserListItem>(queryList, criteria.PagingResult.PageIndex, criteria.PagingResult.PageSize, totalCount);
        }

        public int OptionUser(int id, int optionStatus)
        {
            return UserDataAccess.OptionUser(id,optionStatus);
        }

        #region 接口
        public Api_WxUserLoginResutModel Api_WxUserLoginOrRegister(Api_WxUserLoginCriteria criteria)
        {
            var res = new Api_WxUserLoginResutModel();
            var ds = UserDataAccess.Api_WxUserLoginOrRegister(criteria);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    res.Id = SecretClass.EncryptQueryString(dr["Id"].ToString());
                    res.NickName = dr["NickName"].ToString();   
                    res.HeadImgUrl = dr["HeadImgUrl"].ToString();
                    res.Token = dr["Token"].ToString();
                    res.ShopName = dr["ShopName"].ToString();
                    res.OptionStatus = DataManager.GetRowValue_Int(dr["OptionStatus"]);
                    res.CreateTime = DataManager.GetRowValue_DateTime(dr["CreateTime"]);
                }
                
            }
            return res;
        }

        public WxUserLoginResutModel GetUserSessionModel(int userid)
        {
            var res = new WxUserLoginResutModel();
            var ds = UserDataAccess.GetUserSessionDs(userid);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    res.Id = SecretClass.EncryptQueryString(dr["Id"].ToString());
                    res.NickName = dr["NickName"].ToString();
                    res.HeadImgUrl = dr["HeadImgUrl"].ToString();
                    res.Token = dr["Token"].ToString();
                    res.ShopName = dr["ShopName"].ToString();
                    res.OptionStatus = DataManager.GetRowValue_Int(dr["OptionStatus"]);
                    res.CreateTime = DataManager.GetRowValue_DateTime(dr["CreateTime"]);
                }

            }
            return res;
        }
        #endregion
        public void Dispose()
        {

        }
    }
}
