﻿using SuperiorModel;

namespace SuperiorShopBussinessService
{
    public interface IUserService : IService
    {
        PagedSqlList<UserListItem> Search(UserCriteria criteria);
        int OptionUser(int id, int optionStatus);
        Api_WxUserLoginResutModel Api_WxUserLoginOrRegister(Api_WxUserLoginCriteria criteria);
        WxUserLoginResutModel GetUserSessionModel(int userid);
    }
}
