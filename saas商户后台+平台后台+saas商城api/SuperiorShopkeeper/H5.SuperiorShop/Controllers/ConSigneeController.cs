﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SuperiorModel;
using SuperiorCommon;
using SuperiorShopBussinessService;
using Redis;

namespace H5.SuperiorShop.Controllers
{
    [CheckLoginFitler]
    [JsonException]
    public class ConSigneeController : BaseController
    {
        // GET: ConSignee
        private readonly IConsigneeService _IConsigneeService;
        private readonly IAreaService _IAreaService;

        public ConSigneeController(IConsigneeService IConsigneeService, IAreaService IAreaService)
        {
            _IConsigneeService = IConsigneeService;
            _IAreaService = IAreaService;

        }
        protected override void Dispose(bool disposing)
        {
            this._IConsigneeService.Dispose();
            this._IAreaService.Dispose();
            base.Dispose(disposing);
        }

        [CheckUrlFitler]
        public ActionResult ConSigneeList()
        {
            string _userid = UserContext.DeUserId;
            if (_userid == null)
                return Error("用户异常");
            var res = _IConsigneeService.GetList(_userid.ToInt());
            return View(res);
        }
        [CheckUrlFitler]
        public ActionResult Add()
        {
            return View();
        }
        public ActionResult Edit(int id)
        {
            string _userid = UserContext.DeUserId;
            var list = _IConsigneeService.GetList(_userid.ToInt());
            var res = list.Where(i => i.Id == id).FirstOrDefault();
            return View(res);
        }
        /// <summary>
        /// 添加收获地址
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        /// 
        [AjaxOnly]
        [HttpPost]
        public JsonResult Add(Api_ConsigneeModel criteria)
        {
            if (string.IsNullOrEmpty(criteria.Address) || string.IsNullOrEmpty(criteria.AreaCode) || string.IsNullOrEmpty(criteria.CityCode) || string.IsNullOrEmpty(criteria.ConsigneeName) || string.IsNullOrEmpty(criteria.ConsigneePhone) || string.IsNullOrEmpty(criteria.ProvinceCode))
                return Error("参数错误");
            if (!ValidateUtil.IsValidMobile(criteria.ConsigneePhone))
                return Error("手机格式错误");
            string _userid = UserContext.DeUserId;
            if (_userid == null)
                return Error("用户异常");
            criteria.UserId = _userid;
            string msg = "";
            var ret = _IConsigneeService.Add(criteria, out msg);
            if (ret != 1)
                return Error(msg);
            return Success(true);
        }
        /// <summary>
        /// 删除地址
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        /// 
        [AjaxOnly]
        [HttpGet]
        public JsonResult Del(int id)
        {
            string _userid = UserContext.DeUserId;
            if (_userid == null)
                return Error("用户异常");
            _IConsigneeService.Del(id, _userid.ToInt());
            return Success(true);
        }
        /// <summary>
        /// 编辑地址
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        /// 
        [AjaxOnly]
        [HttpPost]
        public JsonResult Edit(Api_ConsigneeModel criteria)
        {
            if (string.IsNullOrEmpty(criteria.Address) || string.IsNullOrEmpty(criteria.AreaCode) || string.IsNullOrEmpty(criteria.CityCode) || string.IsNullOrEmpty(criteria.ConsigneeName) || string.IsNullOrEmpty(criteria.ConsigneePhone) || string.IsNullOrEmpty(criteria.ProvinceCode) || criteria.Id == 0)
                return Error("参数错误");
            if (!ValidateUtil.IsValidMobile(criteria.ConsigneePhone))
                return Error("手机格式错误");
            string _userid = UserContext.DeUserId;
            if (_userid == null)
                return Error("用户异常");
            criteria.UserId = _userid;
            _IConsigneeService.Edit(criteria);
            return Success(true);
        }
        /// <summary>
        /// 更改地址状态
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userid"></param>
        /// <param name="isdefault"></param>
        /// <returns></returns>
        /// 

        [AjaxOnly]
        [HttpGet]
        public JsonResult UpdateIsDefault(int id, int isdefault)
        {
            string _userid = UserContext.DeUserId;
            if (_userid == null)
                return Error("用户异常");
            _IConsigneeService.UpdateIsDefault(id, _userid.ToInt(), isdefault);
            return Success(true);
        }
        /// <summary>
        /// 获取地址列表
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        /// 
        [AjaxOnly]
        public JsonResult GetList()
        {
            string _userid = UserContext.DeUserId;
            if (_userid == null)
                return Error("用户异常");
            var res = _IConsigneeService.GetList(_userid.ToInt());
            return Success(res);
        }
        /// <summary>
        /// 根据父ID获取地区
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        /// 
        [AjaxOnly]
        public JsonResult GetAreaList(int parentId)
        {
            Func<List<AreaModel>> fucc = delegate ()
            {
                return _IAreaService.GetAll();
            };
            var allAreas = DataIntegration.GetData<List<AreaModel>>("AllAreas", fucc, 9999);
            var res = allAreas.Where(p => p.Parent_Id == parentId);
            return Success(res);
        }
    }
}