﻿using SuperiorModel;
using System.Collections.Generic;
using System.Web.Mvc;
using SuperiorShopBussinessService;

namespace H5.SuperiorShop.Controllers
{
    [CheckLoginFitler]
    [JsonException]
    public class CartController : BaseController
    {
        private readonly IShopCartService _IShopCartService;

        public CartController(IShopCartService IShopCartService)
        {
            _IShopCartService = IShopCartService;

        }
        protected override void Dispose(bool disposing)
        {
            this._IShopCartService.Dispose();
            base.Dispose(disposing);
        }
        // GET: Cart
        public ActionResult Index()
        {
             
            return View();
        }
        /// <summary>
        /// 获取购物车列表模型
        /// </summary>
        /// <returns></returns>
        [CheckUrlFitler]
        [AjaxOnly]
        public JsonResult GetList()
        {
            var parms = base.GetParms();
            if (parms.Spid == 0)
                return Error("非法参数");
            var userid = UserContext.DeUserId;
            var key = "Cart_" + parms.Spid + "_" + userid;
            var res = CartManager.Instance.GetList(key);
            return Success(res);
        }
        /// <summary>
        /// 设置购物车列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        [CheckLoginFitler]
        [HttpPost]
        public JsonResult Set(List<CartItem> model)
        {
            if (model == null)
                model = new List<CartItem>();
            var parms = base.GetParms();
            if (parms.Spid == 0)
                return Error("非法参数");
            var userid = UserContext.DeUserId;
            var key = "Cart_" + parms.Spid + "_" + userid;
            CartManager.Instance.Set(key, model);
            return Success(true);
        }

        /// <summary>
        /// 通过用户本地购物车存储的skuids比如1,2,3来换取最新的购物车所需商品模型的集合
        /// </summary>
        /// <param name="skuids"></param>
        /// <returns></returns>
        /// 

        [CheckLoginFitler]
        [AjaxOnly]
        public JsonResult GetShopCartModelList(string skuids)
        {
            string _userid = UserContext.DeUserId;
            var res = _IShopCartService.Api_GetShopCartList(skuids);
            if (res == null)
                return Error("非法参数");
            return Success(res);
        }
    }
}