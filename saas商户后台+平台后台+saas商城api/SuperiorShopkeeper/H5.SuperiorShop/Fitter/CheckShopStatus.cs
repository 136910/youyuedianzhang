﻿using SuperiorModel;
using System;
using System.Web;
using System.Web.Mvc;
using Redis;
using SuperiorCommon;
using SuperiorShopBussinessService;
using System.Text;

namespace H5.SuperiorShop
{
    /// <summary>
    /// 是否进行跳转登录过滤器
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = true)]
    public class CheckShopStatus : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var _spid =SecretClass.DecryptQueryString(HttpContext.Current.Request["spid"]);
            Func<ShopAppModel> fucc = delegate ()
            {
                return new ShopAppService().GetShopAppModel(_spid.ToInt());
            };
            var shopAppModel = DataIntegration.GetData<ShopAppModel>("GetShopAppModel_" + _spid, fucc);
            if (shopAppModel.OptionStatus == 2)
            {
                filterContext.Result = new RedirectResult("/Error/Index?msg=" + HttpUtility.UrlEncode("该商城已被冻结", Encoding.UTF8));
                return;
            }
            else if (shopAppModel.H5IsExpire)
            {
                filterContext.Result = new RedirectResult("/Error/Index?msg=" + HttpUtility.UrlEncode("该商城使用已到期,请付费!", Encoding.UTF8));
                return;
            }
        }
    }
}