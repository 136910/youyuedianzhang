﻿using SuperiorCommon;
using System;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace H5.SuperiorShop
{
    /// <summary>
    /// 检测URL参数是否合法
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = true)]
    public class CheckUrlFitler : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //检测是否篡改了参数
            if (string.IsNullOrEmpty(HttpContext.Current.Request["spid"]) || string.IsNullOrEmpty(HttpContext.Current.Request["said"]))
            {
                filterContext.Result = new RedirectResult("/Error/Index?msg=" + HttpUtility.UrlEncode("请不要随意改动网站参数", Encoding.UTF8));
                return;
            }
            int spid = 0;
            if (!int.TryParse(SecretClass.DecryptQueryString(HttpContext.Current.Request["spid"]), out spid))
            {
                filterContext.Result = new RedirectResult("/Error/Index?msg=" + HttpUtility.UrlEncode("请不要随意改动网站参数", Encoding.UTF8));
                return;
            }
            int said = 0;
            if (!int.TryParse(SecretClass.DecryptQueryString(HttpContext.Current.Request["said"]), out said))
            {
                filterContext.Result = new RedirectResult("/Error/Index?msg=" + HttpUtility.UrlEncode("请不要随意改动网站参数", Encoding.UTF8));
                return;
            }

        }
    }
}