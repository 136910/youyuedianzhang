﻿(function () {
    EditClass = {};
    var islock = false;//防止同时提交两次数据
    //var _currentNames = [];
    var _currentValues = $("#area").attr("data-codes").split(',');
    var _spid = ToolManager.Common.UrlParms("spid");
    var _said = ToolManager.Common.UrlParms("said");
    EditClass.Instancee = {
        Init: function () {
            $(function () {
                $("#area").cityPicker({
                    title: "选择省市区",
                    onChange: function (picker, values, displayValues) {
                        _currentValues = values;

                    }
                });
            })
            $(document).on('click', '#showTooltips', this.Edit);
            $(document).on('click', '#delteAdd', this.Delete);
        },
        Delete:function(){
            if (confirm("确定删除么?")) {
                var index = layer.load(1);
                $.get("/ConSignee/Del?id=" + $(this).attr("aid") + "&spid="+_spid+"&r=" + new Date().getTime(), function (data) {
                    layer.close(index);
                    if (data.IsSuccess) {
                        layer.msg("删除成功!");
                        window.location.href = document.referrer
                    } else {
                        layer.msg(data.Message);
                    }
                });
            }
        },
        Edit: function () {
            if (islock)
                return false;
            islock = true;
            var consigneeName = $("#consigneeName").val();
            var consigneePhone = $("#consigneePhone").val();
            var address = $("#address").val();
            var zipCode = $("#zipCode").val();
            if (consigneeName == "") {
                layer.msg("收货人必填");
                islock = false;
                return false;
            }
            if (consigneePhone == "") {
                layer.msg("收货人手机必填");
                islock = false;
                return false;
            }
            if (address == "") {
                layer.msg("收货地址必填");
                islock = false;
                return false;
            }
            if (_currentValues.length <= 0) {
                layer.msg("省市区必选");
                islock = false;
                return false;
            }
            var model = {};
            model.Id = $(this).attr("aid");
            model.ConsigneeName = consigneeName;
            model.ConsigneePhone = consigneePhone;
            model.ZipCode = zipCode;
            model.ProvinceCode = _currentValues[0];
            model.CityCode = _currentValues[1];
            model.AreaCode = _currentValues[2];
            model.Address = address;

            var index = layer.load(1);
            RequestManager.Ajax.Post("/ConSignee/Edit?spid="+_spid, model, true, function (data) {
                layer.close(index);
                islock = false;
                if (data.IsSuccess) {
                    layer.msg("编辑成功!");
                    window.location.href = document.referrer
                } else {
                    layer.msg(data.Message);
                }
            })
        }

    }
})()