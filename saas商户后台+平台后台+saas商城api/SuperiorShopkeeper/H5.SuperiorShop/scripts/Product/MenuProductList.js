﻿(function () {
    MenuProductList = {};
    var isScrollLoad = false;
    var loadType = 0;//请求数据方式，0，下拉分页加载，1搜索
    var winH = $(window).height(); //页面可视区域高度
    var lastRequestCount = 8;
    var _spid = ToolManager.Common.UrlParms("spid");
    var _said = ToolManager.Common.UrlParms("said");
    var criteria = {
        Spid: ToolManager.Common.UrlParms("spid"),
        OffSet: 0,
        Size: 8,
        MenuId: ToolManager.Common.UrlParms("menuid"),
        keyWord: ""
    };
    MenuProductList.Instance = {
        Init: function () {

            $(window).scroll(MenuProductList.Instance.ScrollHandler);
            $(document).on('keypress', '#searchInput', function (e) {
                var keycode = e.keyCode;
                loadType = 1;
                if (keycode == '13') {
                    e.preventDefault();
                    criteria.OffSet = 0;
                    MenuProductList.Instance.Request();

                }
            });
            $(function () {
                MenuProductList.Instance.AddChoseClass(criteria.MenuId);
                MenuProductList.Instance.Request();
            })
            $(document).on('click', '.choseMenu', this.ChoseMenu);
        },
        ChoseMenu: function () {
            var menuid = $(this).attr("MenuId");
            MenuProductList.Instance.AddChoseClass(menuid);
            criteria.OffSet = 0;
            criteria.MenuId = menuid;
            loadType = 1;
            MenuProductList.Instance.Request();
        },
        AddChoseClass: function (menuid) {

            $(".choseMenu").each(function () {
                if ($(this).attr("MenuId") == menuid) {
                    $(".itemOn").removeClass("itemOn");
                    $(this).find("div[class='leftItemText']").addClass("itemOn");
                }


            })
        },
        ScrollHandler: function () {
            if (isScrollLoad || lastRequestCount < criteria.Size) {
                return false;
            }

            var pageH = $(document.body).height();
            var scrollT = $(window).scrollTop(); //滚动条top   
            var aa = (pageH - winH - scrollT) / winH;
            if (aa < 0.3) {//0.02是个参数  
                isScrollLoad = true;
                loadType = 0;
                MenuProductList.Instance.Request();
            }
        },
        Request: function () {
            if (loadType == 0)
                $(".weui-loadmore").show();
            criteria.keyWord = $.trim($("#searchInput").val());

            RequestManager.Ajax.Post("/Product/MenuProductList?spid=" + _spid, criteria, true, function (data) {
                $(".weui-loadmore").hide();
                if (data.IsSuccess) {


                    lastRequestCount = data.Data.length;
                    if (data.Data.length > 0) {
                        criteria.OffSet = criteria.OffSet + 1;
                        var said = ToolManager.Common.UrlParms("said");
                        var spid = ToolManager.Common.UrlParms("spid");
                        var htmlArr = [];
                        data.Data.forEach((v) => {
                            htmlArr.push('<li>');
                            htmlArr.push('<a href="/Product/Detail?spid=' + spid + '&said=' + said + '&id=' + v.Id + '">');
                            htmlArr.push('<div class="proimg"><img style="width: 90%;" src="' + v.ImgPath + '"></div>');
                            htmlArr.push('<div class="protxt">');
                            htmlArr.push('<div class="name">' + v.ProductName + '</div>');
                            htmlArr.push('<div class="wy-pro-pri">¥<span>' + v.SallPrice + '</span></div>');
                            htmlArr.push('</div>');
                            htmlArr.push('</a>');
                            htmlArr.push('</li>');
                        });
                        if (loadType == 0)
                            $("#list").append(htmlArr.join(''));
                        else
                            $("#list").html(htmlArr.join(''));
                    } else {
                        //搜索
                        if (loadType == 1)
                            $("#list").html(' <div style="text-align:center">暂无商品</div>');
                    }
                    isScrollLoad = false;
                } else {
                    layer.alert(data.Message);
                }
            })

        },


    };

})();