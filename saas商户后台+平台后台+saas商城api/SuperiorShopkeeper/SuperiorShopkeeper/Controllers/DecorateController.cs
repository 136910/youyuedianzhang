﻿using Newtonsoft.Json.Linq;
using Redis;
using SuperiorCommon;
using SuperiorModel;
using SuperiorShopBussinessService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SuperiorShopkeeper.Controllers
{
    [JsonException]
    [CheckLoginFitler]
    public class DecorateController : BaseController
    {
        private readonly IProductService _IProductService;
        private readonly IShopAppService _IShopAppService;
        private readonly ISearchWordService _ISearchWordService;
        private readonly IDecorateService decorateService;

        public DecorateController(IProductService IProductService, IShopAppService IShopAppService, ISearchWordService ISearchWordService, IDecorateService decorateService)
        {
            _IProductService = IProductService;
            _IShopAppService = IShopAppService;
            _ISearchWordService = ISearchWordService;

            this.decorateService = decorateService;

        }
        protected override void Dispose(bool disposing)
        {
            this._IProductService.Dispose();
            this._IShopAppService.Dispose();
            this._ISearchWordService.Dispose();
            this.decorateService.Dispose();
            base.Dispose(disposing);
        }

        // GET: Decorate
        public ActionResult Index()
        {
            var isExistShop = _IShopAppService.IsExistShopByAdminId(UserContext.DeSecretId.ToInt());
            ViewBag.IsExist = isExistShop;
            return View();
        }

        public ActionResult _Index()
        {

            return View();
        }

        /// <summary>
        /// 读取装修设计数据
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetDesign()
        {
            var model = decorateService.GetDecorateModel(UserContext.DeSecretId.ToInt());
            return Success(model);
        }

        /// <summary>
        /// 保存
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        [ShopAuthority(AuthorityOptionEnum.SaveDecorate)]
        public JsonResult Save(DecorateModel model)
        {
            model.ShopAdminId = UserContext.DeSecretId.ToInt();
            model.Id = model.Id == "0" ? "0" : base.ValidataParms(model.Id);
            decorateService.CreateOrEditDecorate(model);
            if (model.Id != "0")
                RedisManager.Remove("GetDecorateModel_" + model.ShopAdminId);
            return Success(true);
        }

        /// <summary>
        /// 获取首页数据
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetHomeData()
        {
            Func<Api_HomeDataModel> fucc = delegate ()
            {
                return _IProductService.Api_GetHomeData(UserContext.DeSecretId.ToInt());
            };
            var res = DataIntegration.GetData<Api_HomeDataModel>("GetHomeData_" + UserContext.DeSecretId.ToInt(), fucc, 1);
            return Success(res);
        }

        /// <summary>
        /// 获取轮播图数据
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetBanners()
        {
            Func<List<Api_ProductItem>> fucc = delegate ()
            {
                return _IProductService.Api_GetBanners(UserContext.DeSecretId.ToInt());
            };
            var res = DataIntegration.GetData<List<Api_ProductItem>>("GetBanners_" + UserContext.DeSecretId.ToInt(), fucc, 1);
            return Success(res);
        }

        /// <summary>
        /// 获取热门商品数据
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetHotProducts()
        {
            Func<List<Api_ProductItem>> fucc = delegate ()
            {
                return _IProductService.Api_GetHotProducts(UserContext.DeSecretId.ToInt());
            };
            var res = DataIntegration.GetData<List<Api_ProductItem>>("GetHotProducts_" + UserContext.DeSecretId.ToInt(), fucc, 1);
            return Success(res);
        }

        /// <summary>
        /// 获取推荐商品数据
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetCommandProducts()
        {
            Func<List<Api_ProductItem>> fucc = delegate ()
            {
                return _IProductService.Api_GetCommandProducts(UserContext.DeSecretId.ToInt());
            };
            var res = DataIntegration.GetData<List<Api_ProductItem>>("GetCommandProducts_" + UserContext.DeSecretId.ToInt(), fucc, 1);
            return Success(res);
        }

        /// <summary>
        /// 获取目录
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetMenuList()
        {
            Func<List<Api_MenuItemModel>> fuccMenu = delegate ()
            {
                return _IProductService.Api_GetMenuList(UserContext.DeSecretId.ToInt());
            };
            var menuList = DataIntegration.GetData<List<Api_MenuItemModel>>("GetMenuList_" + UserContext.DeSecretId.ToInt(), fuccMenu);
            return Success(menuList);
        }

        /// <summary>
        /// 获取搜索词列表
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetWords()
        {
            Func<List<SearchWordModel>> fucc = delegate ()
            {
                return _ISearchWordService.GetList(UserContext.DeSecretId.ToInt());
            };
            var res = DataIntegration.GetData<List<SearchWordModel>>("GetSearchKeyword_" + UserContext.DeSecretId.ToInt(), fucc, 24);
            res = res.Where(p => p.OptionStatus == 1).ToList();
            return Success(res);
        }

        /// <summary>
        /// 获取首页专场/专区数据
        /// </summary>
        /// <param name="said"></param>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetSpecialPlaceData(int type = 1)
        {
            //读取商品主体
            Func<List<SpecialPlaceModel>> fucc = delegate ()
            {
                return _IProductService.SpecialPlaceList(UserContext.DeSecretId.ToInt(), type);
            };
            var res = DataIntegration.GetData<List<SpecialPlaceModel>>("SpecialPlaceList" + UserContext.DeSecretId.ToInt() + "_" + type, fucc);
            return Success(res);
        }


    }
}