﻿(function () {
    LayoutClass = {};
    LayoutClass.Instance = {
        Init: function () {
            $(document).on('click', '#btn_exit', this.Exit);
            $(function () {
                setInterval(LayoutClass.Instance.CheckVersion, 1000 *15*10);
            })
            $(document).on('click', '#btn_cashOutIng', this.CashOut);
        },
        CashOut:function(){
            layer.alert("提现服务在官方公众号yydz1118,请各位商户务必关注,以便于提供更多服务,谢谢配合!");
        },
        //检测小程序是否有版本更新，方法检测cookie
        CheckVersion:function(){
            $.get("/Home/CheckVersion?tm=" + new Date().getSeconds(), null, function (data) {
                if (data.IsSuccess) {
                    if (data.Data != "") {
                        //检测当前页面是否已经提示通知，有的话，就不再提示了
                        if ($("#pro_notice").html() == undefined) {
                            var index = layer.open({
                                type: 1,
                                title: false,
                                closeBtn: 1, //不显示关闭按钮
                                shade: [0],
                                area: ['340px', '215px'],
                                offset: 'rb', //右下角弹出
                                time: 0, //2秒后自动关闭
                                anim: 2,
                                btn: ['知道了'],
                                content: LayoutClass.Instance.CreateNoticeHtml(data.Data), //iframe的url，no代表不显示滚动条
                                end: function () {

                                },
                                yes: function () {
                                    //记录当前版本cookie，不再弹出，通过请求方式
                                    $.get("/Home/SetNoticeCookie?tm=" + new Date().getSeconds(), null, function (data) {
                                        if (data.IsSuccess)
                                            layer.close(index);
                                        else
                                            layer.alert(data.Message);
                                    })

                                }
                            });
                        }

                    }
                    
                } else {
                    //layer.alert(data.Message);
                }
                   
                        
            })
        },
        
        CreateNoticeHtml: function (msg) {
            var res = [];
            res.push('<div id="pro_notice" class="layui-card">');
            res.push(' <div class="layui-card-header">');
            res.push('小程序版本更新通知 </div>');
            res.push(' <div class="layui-card-body layui-text layadmin-text">' + msg + '</div>');
            res.push('</div>');
            return res.join('');
        },
        //退出
        Exit: function () {
            $.get("/Login/Exist?tm="+new Date().getSeconds(), null, function (data) {
                if (data.IsSuccess)
                    window.location.href = "/Login/Login";
                else
                    layer.alert(data.Message);
            })
        }
    };
})()