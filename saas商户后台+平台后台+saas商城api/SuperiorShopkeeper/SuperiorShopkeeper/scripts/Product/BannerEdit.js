﻿(function () {
    BannerEditClass = {};
    var islock = false;//防止同时提交两次数据
    var _index;
    BannerEditClass.Instance = {
        Init: function () {
            //layui资源加载，验证
            layui.use('form', function () {
                var form = layui.form;
                form.render();
                form.on('submit(add)', BannerEditClass.Instance.Submit);
            });
            //layui上传图片资源加载
            layui.use('upload', function () {
                var upload = layui.upload;
                //执行实例
                var uploadInst = upload.render({
                    elem: '#btn_upload', //绑定元素
                    url: '/Upload/UploadImg?object=ProductBanner', //上传接口
                    size: 1500,
                    done: function (res) {
                        //上传完毕回调
                        if (res.code == "0") {
                            BannerEditClass.Instance.CreateBannerImg(res.data.src);
                        } else {
                            layer.alert(res.msg);
                        }
                    },
                    error: function () {
                        //请求异常回调
                        layer.alert("图片上传异常");
                    }
                });
            });
            $(document).on('click', '.imgDelete', this.DeleteImg);
            $(document).on('click', '#btnBack', this.Back);
        },
        //删除图
        DeleteImg: function () {
            var that = $(this);
            layer.confirm('确定要删除么', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                that.parent().remove();
                layer.closeAll('dialog');
            }, function () {

            });
        },
        //返回
        Back: function () {
            layer.confirm('数据不会保存,确认后返回列表页?', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                window.location.href = "/Product/BannerList";
            }, function () {

            });
        },
        //动态创建图层
        CreateBannerImg: function (src) {
            var arr = [];
            arr.push('<div class="img_div">');
            arr.push('<img class="layui-upload-img" src="' + src + '">');
            arr.push('<i class="layui-icon layui-icon-close-fill imgDelete"></i>');
            arr.push('</div>');
            $('.layui-upload-list').html(arr.join(''));
        },
        //提交
        Submit: function () {
            if (islock)
                return false;
            islock = true;
            if ($('.layui-upload-list .img_div').length <= 0) {
                layer.alert("请上传封面图");
                islock = false;
                return false;
            }
            var imgpath = $('.layui-upload-list img').attr("src");
            var ProductId = $("#ProductId").val();
            var Sorft = $("#Sorft").val();
            var id = $(this).attr("bannerId");
            var index = layer.load(1);
            RequestManager.Ajax.Post("/Product/EditBanner", {
                "ProductId": ProductId,
                "Sorft": Sorft,
                "ImgPath": imgpath,
                "Id":id

            }, true, function (data) {
                layer.close(index);
                islock = false;
                if (data.IsSuccess) {
                    layer.confirm('添加成功,是否返回列表?', {
                        btn: ['确定', '取消'] //按钮
                    }, function () {
                        window.location.href = "/Product/BannerList";
                    }, function () {

                    });
                } else {
                    layer.alert(data.Message);
                }
            })
        },


    };
})();