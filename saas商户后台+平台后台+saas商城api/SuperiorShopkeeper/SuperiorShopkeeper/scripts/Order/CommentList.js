﻿(function () {
    CommentListClass = {};
    var _searchCriteria = { PagingResult: { PageIndex: 0, PageSize: 20 }};
    CommentListClass.Instance = {
        Init: function () {
            layui.use('form', function () {
                var form = layui.form;
                form.render();
                
            });
  
            //分页事件注册
            superior.ui.control.Pager.enablePaging(document, CommentListClass.Instance.Refresh);
            $(document).on('click', '#btn_search', this.Search);
            $(document).on('click', '.showMsg', this.ShowMsg);
        },
        ShowMsg:function(){
            var msg = $(this).attr("Msg");
            layer.alert(msg);
        },
        Search: function () {

            _searchCriteria.OrderNo = $("#search_OrderNo").val();
            _searchCriteria.CommentLevel = $("#search_level").val();
            CommentListClass.Instance.Refresh(0);
        },
        Refresh: function (pageIndex) {
            var url = "/Order/CommentList";
            if (pageIndex !== undefined)
                _searchCriteria.PagingResult.PageIndex = pageIndex;
            var index = layer.load(1);
            RequestManager.Ajax.Post(url, _searchCriteria, true, function (data) {
                layer.close(index);
                $("#dataList").html(data);
            })
        }
    };
})()