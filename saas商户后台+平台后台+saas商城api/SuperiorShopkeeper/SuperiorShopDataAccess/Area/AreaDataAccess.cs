﻿using SuperiorSqlTools;
using System.Data;
using System.Data.SqlClient;


namespace SuperiorShopDataAccess
{
    public class AreaDataAccess
    {
        public static DataSet GetArea(int parentId)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@parentId",parentId)
            };
            var sql = "select * from C_Areas where Parent_Id=@parentId";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }
        public static DataSet GetAllArea()
        {
            var sqlManager = new SqlManager();
            var sql = "select * from C_Areas";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, null);
        }
    }
}
