﻿using Redis;
using SuperiorModel;
using SuperiorShopBussinessService;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Api.WeiXinGroup.Controllers
{
    [ApiExceptionAttribute]
    public class HomeController : ApiControllerBase
    {
        private readonly IArticleService _IArticleService;
        private readonly IGroupDataService _IGroupDataService;

        public HomeController(IArticleService IArticleService, IGroupDataService IGroupDataService)
        {
            _IArticleService = IArticleService;
            _IGroupDataService = IGroupDataService;
        }
        protected override void Dispose(bool disposing)
        {
            this._IArticleService.Dispose();
            this._IGroupDataService.Dispose();
            base.Dispose(disposing);
        }

        /// <summary>
        /// 获取轮播图
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiResultModel<object> BannerList(int pageType)
        {
            Func<List<Api_GroupBannerModel>> func = () =>
            {
                return _IArticleService.Api_GetGroupBannerList(pageType);
            };
            var res = DataIntegration.GetData<List<Api_GroupBannerModel>>("GroupBannerList_Page_"+ pageType, func);
            return Success(res);
        }
        /// <summary>
        /// 获取首页最新2条文章
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public ApiResultModel<object> ArticleList()
        {
            Func<List<Api_ArticleItem>> func = () =>
            {
                return _IArticleService.Api_GetList(0, 2);
            };
            var res = DataIntegration.GetData<List<Api_ArticleItem>>("Group_Article_Home", func);
            return Success(res);
        }
        /// <summary>
        /// 读取公告
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiResultModel<object> AdList()
        {
            Func<List<Api_ArticleItem>> func = () =>
            {
                return _IArticleService.Api_GetAdList();
            };
            var res = DataIntegration.GetData<List<Api_ArticleItem>>("Group_Ad_Home", func);
            return Success(res);
        }

        /// <summary>
        /// 读取首页统计数据
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiResultModel<object> HomeCountData()
        {
            Func<Api_HomeCountData> func = () =>
            {
                return _IGroupDataService.GetHomeCountData();
            };
            var res = DataIntegration.GetData<Api_HomeCountData>("Group_Home_CountData", func,1);
            return Success(res);
        }
    }
}
