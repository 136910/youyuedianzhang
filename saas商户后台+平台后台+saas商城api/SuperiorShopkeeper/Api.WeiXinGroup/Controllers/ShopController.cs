﻿using Redis;
using SuperiorCommon;
using SuperiorModel;
using SuperiorShopBussinessService;
using System;
using System.Web.Http;

namespace Api.WeiXinGroup.Controllers
{
    [ApiExceptionAttribute]
    public class ShopController : ApiControllerBase
    {
        // GET: Shop
        private readonly IGroupShopService _IGroupShopService;

        public ShopController(IGroupShopService IGroupShopService)
        {
            this._IGroupShopService = IGroupShopService;
        }

        protected override void Dispose(bool disposing)
        {
            this._IGroupShopService.Dispose();
            base.Dispose(disposing);
        }

        /// <summary>
        /// 创建商家
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResultModel<object> Create(Api_GroupShopRequestModel model)
        {
            if (!ModelState.IsValid)
                return Error(ErrorMsg());
            var _userid = base.ValidataParms(model.UserId);
            if (_userid == null)
                return Error("非法参数");
            model.UserId = _userid;
            if (!SuperiorCommon.ValidateUtil.IsValidMobile(model.PhoneNumber))
                return Error("请输入正确的手机号");
            //将内容图上传的图片服务器
            if (!UploadService.Upload(model.Images, UploaderKeys.GroupShopImage))
                return Error("上传图片失败");
            //上传logo
            if (!UploadService.Upload(model.Logo, UploaderKeys.GroupShopImage))
                return Error("上传LOGO失败");
            string msg = string.Empty;
            var res = _IGroupShopService.Api_Create(model, out msg);
            if (res != 1)
                return Error(msg);
            return Success(res);
        }

        /// <summary>
        /// 更新商家
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResultModel<object> Update(Api_GroupShopRequestModel model)
        {
            if (!ModelState.IsValid)
                return Error(ErrorMsg());
            if (model.Id <= 0)
                return Error("id异常");
            var _userid = base.ValidataParms(model.UserId);
            if (_userid == null)
                return Error("非法参数");
            model.UserId = _userid;
            if (!SuperiorCommon.ValidateUtil.IsValidMobile(model.PhoneNumber))
                return Error("请输入正确的手机号");
            //将内容图上传的图片服务器
            if (!UploadService.Upload(model.Images, UploaderKeys.GroupShopImage))
                return Error("上传图片失败");
            //上传logo
            if (!UploadService.Upload(model.Logo, UploaderKeys.GroupShopImage))
                return Error("上传LOGO失败");
            string msg = string.Empty;
            var res = _IGroupShopService.Api_Update(model, out msg);
            if (res != 1)
                return Error(msg);
            //控制redis
            RedisManager.Remove("GroupShop_Info_"+model.Id);
            return Success(res);
        }

        /// <summary>
        /// 获取商家列表数据
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResultModel<object> List(Api_GroupShopListCriteria criteria)
        {
            if (!string.IsNullOrEmpty(criteria.UserId))
            {
                var _userid = base.ValidataParms(criteria.UserId);
                if (_userid == null)
                    return Error("非法参数");
                criteria.UserId = _userid;
            }
            var res = _IGroupShopService.Api_GetList(criteria);
            return Success(res);
        }
        /// <summary>
        /// 读取用户收藏的商家列表
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResultModel<object> CollectedList(Api_GroupShopCollectedListCriteria criteria)
        {
            var _userid = base.ValidataParms(criteria.UserId);
            if (_userid == null)
                return Error("非法参数");
            criteria.UserId = _userid;
            var res = _IGroupShopService.Api_GetCollectedList(criteria);
            return Success(res);
        }

        /// <summary>
        /// 根据ID获取单个商家详情数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public ApiResultModel<object> Get(int id)
        {
            Func<Api_GroupShopDetailModel> fucc = delegate ()
            {
                return _IGroupShopService.Api_Get(id);
            };
            var res = DataIntegration.GetData<Api_GroupShopDetailModel>("GroupShop_Info_" + id, fucc);
            return Success(res);
        }
        /// <summary>
        /// 更新信息统计数据
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public ApiResultModel<object> UpdateData(int type, int id)
        {
            _IGroupShopService.Api_UpdateData(type, id);
            return Success(true);
        }

        /// <summary>
        /// 设置商家置顶
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResultModel<object> SetTop(Api_SetTopForShopRequestModel model)
        {
            if (!ModelState.IsValid)
                return Error(ErrorMsg());
            var _userid = base.ValidataParms(model.UserId);
            if (_userid == null)
                return Error("非法参数");
            var msg = string.Empty;
            var ret = _IGroupShopService.Api_SetTop(_userid.ToInt(), model.ShopId, model.TopConfigId, out msg);
            if (ret != 1)
                return Error(msg);
            return Success(true);
        }

    }
}