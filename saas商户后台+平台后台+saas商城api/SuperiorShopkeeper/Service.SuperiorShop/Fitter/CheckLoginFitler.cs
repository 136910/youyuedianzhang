﻿using SuperiorCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Service.SuperiorShop
{
    /// <summary>
    /// 商户是否进行跳转登录过滤器
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = true)]
    public class CheckShopLoginFitler : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (HttpContext.Current.Session[SessionKey.ServiceShopKey] == null)
            {
                filterContext.Result = new RedirectResult("/Shop/Login");
                return;
            }

        }
    }
    /// <summary>
    /// 推广人是否进行跳转登录过滤器
    /// 
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = true)]
    public class CheckMangerAccountLoginFitler : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (HttpContext.Current.Session[SessionKey.ServiceManagerAccountKey] == null)
            {
                filterContext.Result = new RedirectResult("/ManagerAccount/Login");
                return;
            }

        }
    }
}