﻿(function () {
    ProductListClass = {};
    var _searchCriteria = { PagingResult: { PageIndex: 0, PageSize: 15 }, OptionStatus:999 };
    ProductListClass.Instance = {
        Init: function () {
            //分页事件注册
            superior.ui.control.Pager.enablePaging(document, ProductListClass.Instance.Refresh);
            $(document).on('click', '#btn_search', this.Search);
        },
        Search: function () {
            _searchCriteria.ProductName = $("#search_ProductName").val();
            _searchCriteria.Brand = $("#search_Brand").val();
            _searchCriteria.LoginName = $("#search_LoginName").val();
            _searchCriteria.OptionStatus = $("#search_status").val();
            ProductListClass.Instance.Refresh(0);
        },
        Refresh: function (pageIndex) {
            if (pageIndex !== undefined)
                _searchCriteria.PagingResult.PageIndex = pageIndex;
            var index = layer.load(1);
            RequestManager.Ajax.Post("/Product/ProductList", _searchCriteria, true, function (data) {
                layer.close(index);
                $("#dataList").html(data);
            })
        }

    };

})();