﻿(function () {
    RequestManager.Ajax = {
        Post: function (url, data,isAsync,successHandler, errorHandler, options) {
            if (url.indexOf('?') != -1) {
                url += "&r=" + new Date().getTime();
            } else {
                url += "?r=" + new Date().getTime();
            }
            var options = RequestManager.Ajax.getAjaxOptions(url, data, isAsync, successHandler, errorHandler, options);
            var jqXHR = $.ajax(options);
            return jqXHR;
        },
        getAjaxOptions: function (url, data,isAsync, successHandler, errorHandler, options) {
            var ajaxOptions = {
                type: "post",
                contentType: "application/json",
                data: data,
                url: url,
                async:isAsync,
                success: successHandler,
                error: errorHandler
            };
            if (options !== undefined) {
                $.extend(ajaxOptions, options);
            }
            if (ajaxOptions.contentType == "application/json" && ajaxOptions.data != null && typeof data !== "function") {
                ajaxOptions.data = JSON.stringify(data);
            }
            return ajaxOptions;
        }


        
    };
})();