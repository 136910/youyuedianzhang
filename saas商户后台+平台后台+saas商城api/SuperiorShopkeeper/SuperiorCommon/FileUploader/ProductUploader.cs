﻿namespace SuperiorCommon
{
    public class ProductBannerUploader : MinDimensionUploader
    {
        public static readonly ProductBannerUploader Instance = new ProductBannerUploader();
        private ProductBannerUploader() { }

        public override int MinWidth
        {
            get { return 100; }
        }

        public override int MinHeight
        {
            get { return 100; }
        }

        /// <summary>
        /// 50
        /// </summary>
        public override int MaxBytesLength
        {
            get { return 2000 * 1024; }
        }

        public override string[] AllowedImageTypes
        {
            get { return new string[] { ".jpg", ".png",".jpeg" }; }
        }

        public override string FolderName
        {
            get
            {
                return "ProductBanner";
                //    ConfigSettings.Instance.FileUploadFolderNameAd;
            }
        }
    }

    public class ProductDetailUploader : MinDimensionUploader
    {
        public static readonly ProductDetailUploader Instance = new ProductDetailUploader();
        private ProductDetailUploader() { }

        public override int MinWidth
        {
            get { return 100; }
        }

        public override int MinHeight
        {
            get { return 100; }
        }

        /// <summary>
        /// 50
        /// </summary>
        public override int MaxBytesLength
        {
            get { return 1000 * 1024; }
        }

        public override string[] AllowedImageTypes
        {
            get { return new string[] { ".jpg", ".png", ".jpeg" }; }
        }

        public override string FolderName
        {
            get
            {
                return "ProductDetail";
                //    ConfigSettings.Instance.FileUploadFolderNameAd;
            }
        }
    }

    public class ProductMenuUploader : MinDimensionUploader
    {
        public static readonly ProductMenuUploader Instance = new ProductMenuUploader();
        private ProductMenuUploader() { }

        public override int MinWidth
        {
            get { return 100; }
        }

        public override int MinHeight
        {
            get { return 100; }
        }

        /// <summary>
        /// 50
        /// </summary>
        public override int MaxBytesLength
        {
            get { return 1000 * 1024; }
        }

        public override string[] AllowedImageTypes
        {
            get { return new string[] { ".jpg", ".png", ".jpeg" }; }
        }

        public override string FolderName
        {
            get
            {
                return "ProductMenu";
                //    ConfigSettings.Instance.FileUploadFolderNameAd;
            }
        }
    }
}
