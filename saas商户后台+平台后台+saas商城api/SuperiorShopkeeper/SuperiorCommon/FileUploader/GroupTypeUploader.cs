﻿namespace SuperiorCommon
{
    public class GroupTypeUploader:MinDimensionUploader
    {
        public static readonly GroupTypeUploader Instance = new GroupTypeUploader();
        private GroupTypeUploader() { }

        public override int MinWidth
        {
            get { return 10; }
        }

        public override int MinHeight
        {
            get { return 10; }
        }

        /// <summary>
        /// 50
        /// </summary>
        public override int MaxBytesLength
        {
            get { return 2000 * 1024; }
        }

        public override string[] AllowedImageTypes
        {
            get { return new string[] { ".jpg", ".png", ".jpeg" }; }
        }

        public override string FolderName
        {
            get
            {
                return "GroupTypeImage";
                //    ConfigSettings.Instance.FileUploadFolderNameAd;
            }
        }
    }
}
