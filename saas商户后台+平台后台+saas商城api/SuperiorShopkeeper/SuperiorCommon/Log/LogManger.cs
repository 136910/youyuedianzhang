﻿using log4net;
using System.Threading.Tasks;

namespace SuperiorCommon
{
    public class LogManger
    {
        private LogManger() { }
        private static LogManger _instance = new LogManger();
        private static readonly object locker = new object();
        public static LogManger Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (locker)
                    {
                        if (_instance == null)
                        {
                            _instance = new LogManger();
                        }
                    }
                }
                return _instance;
            }
        }

        private static ILog log = log4net.LogManager.GetLogger("log4net");
        public void WriteLog(string str)
        {
            Task.Factory.StartNew(() =>
            {
                log.Info(str);
            });
        }
    }
}
