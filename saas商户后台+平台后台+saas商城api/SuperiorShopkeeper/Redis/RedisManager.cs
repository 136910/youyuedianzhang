﻿using System;
using System.Collections.Generic;
using System.Linq;
using SuperiorCommon;
using ServiceStack.Redis;

namespace Redis
{
    public static class RedisManager
    {
        #region static field
        private static IRedisClient _redisClient = null;
        private static string RedisHost = ConfigSettings.Instance.RedisHost;
        private static int Port = 6379;
        private static string Password = "xxxxxxx";

        private static IRedisClient RedisClient
        {
            get
            {
                if (_redisClient == null)
                {
                    _redisClient = new RedisClient(ConfigSettings.Instance.RedisHost, 6379) { Password = "xxxxxxx" };
                    //_redisClient.Password = "caonimadaohao123";
                }
                return _redisClient;
            }
        }
        #endregion
        #region
        /// <summary>
        /// 获取信息
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <returns>对象</returns>
        public static T Get<T>(string token)
        {
            using (var client = new RedisClient(RedisHost, Port) { Password = Password })
            {
                return client.Get<T>(token);
            }

        }
        /// <summary>
        /// 设置信息
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="token">key</param>
        /// <param name="obj">对象</param>
        public static void Set<T>(string token, T obj, DateTime expressTime)
        {
            using (var client = new RedisClient(RedisHost, Port) { Password = Password })
            {
                client.Set<T>(token, obj, expressTime);
            }


        }
        /// <summary>
        /// 删除单个缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        public static bool Remove(string key)
        {
            using (var client = new RedisClient(RedisHost, Port) { Password = Password })
            {
                return client.Remove(key);
            }

        }
        /// <summary>
        /// 删除多个缓存
        /// </summary>
        /// <param name="keys"></param>
        /// <returns></returns>
        public static bool RemoveList(List<string> keys)
        {
            try
            {
                using (var client = new RedisClient(RedisHost, Port) { Password = Password })
                {
                    client.RemoveAll(keys);
                    return true;
                }

            }
            catch
            {

                LogManger.Instance.WriteLog("redis挂了,请求支援");
                return false;
            }
        }
        /// <summary>
        /// 向指定集合K添加值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void AddItemToListT<T>(string key, T value)
        {
            using (var client = new RedisClient(RedisHost, Port) { Password = Password })
            {
                if (client.Get<List<T>>(key) == null)
                {
                    client.Set<List<T>>(key, new List<T>());
                }
                var list = client.Get<List<T>>(key);
                list.Add(value);
                client.Set<List<T>>(key,list);
            }
        }
        /// <summary>
        /// 获取集合最近一个元素
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        public static T GetLasterItemFromList<T>(string key)
        {
            using (var client = new RedisClient(RedisHost, Port) { Password = Password })
            {
               return client.Get<List<T>>(key).LastOrDefault();
            }
        }
        #endregion
    }
}
