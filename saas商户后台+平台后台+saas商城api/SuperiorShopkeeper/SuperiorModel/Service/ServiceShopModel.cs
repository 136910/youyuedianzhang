﻿using System.ComponentModel.DataAnnotations;

namespace SuperiorModel
{
    class ServiceShopModel
    {
    }

    public class Shop_SubmitAmountParms
    {
        [Required(ErrorMessage = "验证码不能为空")]
        public string Code { get; set; }
        [RegularExpression(@"(^[1-9](\d+)?(\.\d{1,2})?$)|(^0$)|(^\d\.\d{1,2}$)", ErrorMessage = "提现金额不合法")]
        public decimal Amount { get; set; }
    }

    public class SubmitCashOutResultModel
    {
        public string OpenId { get; set; }
        public string ExpressAmount { get; set; }
    }

    public class ShopCashOutModel
    {
        public int Id { get; set; }
        public string NickName { get; set; }
        public string OpenId { get; set; }
        public string OrderNo { get; set; }
        public string Amount { get; set; }
        public string CreateTime { get; set; }
        public string PayTime { get; set; }
        public int OrderStatus { get; set; }
        public string Remark { get; set; }
        public string LoginName { get; set; }

    }
}
