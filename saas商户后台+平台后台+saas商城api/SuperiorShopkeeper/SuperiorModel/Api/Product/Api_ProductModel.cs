﻿using System.Collections.Generic;

namespace SuperiorModel
{

    public class Api_MenuItemModel
    {
        public int Id { get; set; }
        public string MenuName { get; set; }
        public string ImgPath { get; set; }
        public int IsAll { get; set; }
    }
   
    public class Api_ProductDetailModel
    {
        public Api_ProductDetailModel()
        {
            this.SkuManagerModel = new ProductSkuManagerModel();
        }
        public string Id { get; set; }
        public string ProductName { get; set; }
        public string Brand { get; set; }
        public string Title { get; set; }
        public int MenuId { get; set; }
        public int ShipTemplateId { get; set; }
        public string ShipAmount { get; set; }
        public string Unit { get; set; }
        public string Detail { get; set; }
        public List<string> BannerImgs { get; set; }
        public decimal MinAmount { get; set; }
        public decimal MaxAmount { get; set; }
        public int CurrentSkuType { get; set; }
        public string AmountRank
        {
            get
            {
                return MinAmount.ToString() + " - " + MaxAmount.ToString();
            }
        }

        public ProductSkuManagerModel SkuManagerModel { get; set; }
    }

   

    

}
