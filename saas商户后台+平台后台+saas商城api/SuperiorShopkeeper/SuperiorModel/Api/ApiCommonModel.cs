﻿using System.Collections.Generic;
using System.Linq;

namespace SuperiorModel
{
    public class Api_CountShipAmountCriteria
    {
        public List<Api_CountShipItem> Products { get; set; }
        public string Spid { get; set; }
        public string UserId { get; set; }
        public decimal TotalProductAmount { get; set; }
        public string ProvinceCode { get; set; }
    }

    public class Api_CountShipItem
    {
        public string ProvinceCode { get; set; }
        public string ProductName { get; set; }
        public string ProductId { get; set; }
        public int SaleNum { get; set; }
        public int Product_SkuId { get; set; }
        public float Weight { get; set; }
        public int ShippingTemplateId { get; set; }
        public ShippingTemplateModel ShippingTemplateModel { get; set; }
        /// <summary>
        /// 该商品的运费模板下，送达的所有省份，
        /// </summary>
        public List<string> ShipAllProvinceIds
        {
            get
            {
                var res = new List<string>();
                if (this.ShippingTemplateModel == null)
                    return res;
                this.ShippingTemplateModel.Items.ForEach((item) =>
                {
                    res.AddRange(item.ProvinceIdList);
                });
                return res;
            }
        }
        /// <summary>
        /// 该商品，所属当前收货人省份的子模板
        /// </summary>
        public ShippingTemplateItem CurrentShipItem
        {
            get
            {
                if (this.ShippingTemplateModel == null)
                    return null;
                return this.ShippingTemplateModel.Items.Where(i => i.ProvinceIdList.Contains(this.ProvinceCode)).FirstOrDefault();
            }
        }
    }

}
