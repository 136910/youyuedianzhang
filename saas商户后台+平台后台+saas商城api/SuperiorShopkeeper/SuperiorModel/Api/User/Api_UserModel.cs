﻿using System;

namespace SuperiorModel
{
    public class Api_WxUserLoginResutModel
    {
        public string Id { get; set; }
        public string NickName { get; set; }
        public string HeadImgUrl { get; set; }
        public int OptionStatus { get; set; }
        public DateTime CreateTime { get; set; }
        public string Token { get; set; }
        public string ShopName { get; set; }
    }
    /// <summary>
    /// H5页面专用
    /// </summary>
    [Serializable]
    public class WxUserLoginResutModel
    {
        public WxUserLoginResutModel() { }

        public WxUserLoginResutModel(Api_WxUserLoginResutModel user)
        {
            this.Id = user.Id;
            this.NickName = user.NickName;
            this.HeadImgUrl = user.HeadImgUrl;
            this.OptionStatus = user.OptionStatus;
            this.CreateTime = user.CreateTime;
            this.Token = user.Token;
            this.ShopName = user.ShopName;
        }
        public string Id { get; set; }
        public string NickName { get; set; }
        public string HeadImgUrl { get; set; }
        public int OptionStatus { get; set; }
        public DateTime CreateTime { get; set; }
        public string Token { get; set; }
        public string ShopName { get; set; }
    }

    public class Api_WxUserLoginCriteria
    {
        public string Spid { get; set; }
        public string Code { get; set; }
        public string HeadImgUrl { get; set; }
        public int Sex { get; set; }
        public string NickName { get; set; }
        public int AppType { get; set; }
        public string OpenId { get; set; }

    }

    public class Api_OrderCountModel
    {
        public int WaitPayCount { get; set; }

        public int SendCount { get; set; }
        public int WaitSendCount { get; set; }
        public int FailCount { get; set; }
        public int ServiceCount { get; set; }
    }
}
