﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace SuperiorModel
{
    public class DecorateConfigModel
    {
        public string Name { get; set; }
        public bool IsRender { get; set; }
    }

    public class DecorateModel
    {
        public DecorateModel()
        {
            this.DecorateDesign = new List<DesignItem>();
        }
        
        public string Id { get; set; } = "0";
        public int ShopAdminId { get; set; }
        public List<DesignItem> DecorateDesign { get; set; }
    }

    public class DesignItem
    {
        public string Name { get; set; }
        public bool IsRender { get; set; }
        public string Color { get; set; } = "";

    }
}
