﻿using System;

namespace SuperiorModel
{
    public class SearchWordModel
    {
        public int Id { get; set; }
        public int ShopAdminId { get; set; }
        public string KeyWord { get; set; }
        public int OptionStatus { get; set; }
        public DateTime CreateTime { get; set; }

    }
}
