﻿using System;
using System.Collections.Generic;

namespace SuperiorModel
{
    [Serializable]
    public class AuthorityModel
    {
        public AuthorityModel()
        {
            this.checkArr = new List<AuthorityCheckItem>();
        }
        public AuthorityModel(int id, string title, int parentId, AuthorityCheckItem item)
        {
            this.checkArr = new List<AuthorityCheckItem>();
            this.id = id;
            this.title = title;
            this.parentId = parentId;
            checkArr.Add(item);
        }
        public int id { get; set; }
        public string title { get; set; }
        public int parentId { get; set; }
        public List<AuthorityCheckItem> checkArr { get; set; }
    }
    [Serializable]
    public class AuthorityCheckItem
    {
        public string type { get; set; }
        public string isChecked { get; set; }

    }
}
