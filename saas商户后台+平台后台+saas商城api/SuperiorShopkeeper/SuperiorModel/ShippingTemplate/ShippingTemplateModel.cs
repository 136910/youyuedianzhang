﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace SuperiorModel
{

    public class ShippingTemplateModel
    {
        public ShippingTemplateModel()
        {
            this.Items = new List<ShippingTemplateItem>();
        }
        public int Id { get; set; }
        [Required(ErrorMessage = "运费模板名称不能为空")]
        public string ShippingTemplatesName { get; set; }
        public int ChargType { get; set; }
        public int ShopAdminId { get; set; }

        public DateTime CreateTime { get; set; }

        public List<ShippingTemplateItem> Items { get; set; }
    }
    
    public class ShippingTemplateItem
    {
        public int Id { get; set; }
        public int ShippingTemplateId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// 
        [Required(ErrorMessage = "区域选择不能为空")]
        public string ProvinceIds { get; set; }
        [Required(ErrorMessage = "区域名称不能为空")]
        public string ProvinceStr { get; set; }
        [RegularExpression(@"^\d+$", ErrorMessage = "首件(重)如果不填,请写0")]
        public int FirstValue { get; set; }
        [RegularExpression(@"(^[1-9](\d+)?(\.\d{1,2})?$)|(^0$)|(^\d\.\d{1,2}$)", ErrorMessage = "首件(重)运费不填，请写0")]
        public decimal FirstAmount { get; set; }
        [RegularExpression(@"^\d+$", ErrorMessage = "续件(重)如果不填,请写0")]
        public int NextValue { get; set; }
        [RegularExpression(@"(^[1-9](\d+)?(\.\d{1,2})?$)|(^0$)|(^\d\.\d{1,2}$)", ErrorMessage = "续件(重)运费不填，请写0")]
        public decimal NextAmount { get; set; }

        public List<string> ProvinceIdList
        {
            get
            {
                if (string.IsNullOrEmpty(this.ProvinceIds))
                {
                    return new List<string>();
                }
                else
                {
                    return this.ProvinceIds.Split(',').ToList();
                }

            }
        }

    }
}
