﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperiorModel
{
    public class CourseMainCriteria: PagedBaseModel
    {
        public string  Title { get; set; }
        public long? ClassifyId { get; set; }
    }
}
