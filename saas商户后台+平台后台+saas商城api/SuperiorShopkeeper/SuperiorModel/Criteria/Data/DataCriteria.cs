﻿namespace SuperiorModel
{
    class DataCriteria
    {
    }
    public class ShopOrderRankCriteria:PagedBaseModel
    {
        public string LoginName { get; set; }
    }
    public class ShopUserRankCriteria : PagedBaseModel
    {
        public string LoginName { get; set; }
    }
    public class ShopAppExpressRankCriteria : PagedBaseModel
    {
        public string LoginName { get; set; }
        public int AppType { get; set; }
    }
}
