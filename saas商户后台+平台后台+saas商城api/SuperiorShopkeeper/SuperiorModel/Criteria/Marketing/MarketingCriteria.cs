﻿namespace SuperiorModel
{
    public class MarketingCriteria
    {
    }

    public class UserCouponCriteria : PagedBaseModel
    {
        public int ShopAdminId { get; set; }
        public string NickName { get; set; }
        public int CouponType { get; set; }

    }

    public class AdminCouponCriteria : PagedBaseModel
    {
        public string  LoginName { get; set; }
        public int CouponType { get; set; }
    }
}
