﻿namespace SuperiorModel
{
    public class OrderCriteria : PagedBaseModel
    {
        public string OrderNo { get; set; }
        public int AppType { get; set; }
        public int ShopAdminId { get; set; }
        public string ShopAppId { get; set; }
        public string BeginTime { get; set; }
        public string EndTime { get; set; }
        /// <summary>
        /// 0，待付款，1，（已发货）订单成功，2订单失败（15分钟未支付），3已付款，4申请售后订单，
        /// </summary>
        public int OrderStatus { get; set; }
        public string LoginName { get; set; }
    }

    public class Api_OrderListCriteria
    {
        public string Spid { get; set; }
        public string UserId { get; set; }
        public int OffSet { get; set; }
        public int Size { get; set; }
        /// <summary>
        /// 0，待付款，1，（已发货）订单成功，2订单失败（15分钟未支付），3已付款，4申请售后订单，999全部
        /// </summary>
        public int OrderStatus { get; set; }
    }
}
