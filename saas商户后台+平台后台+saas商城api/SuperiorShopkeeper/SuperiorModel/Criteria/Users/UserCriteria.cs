﻿namespace SuperiorModel
{
    public class UserCriteria:PagedBaseModel
    {
        public string NickName { get; set; }
        public int AppType { get; set; }
        public int ShopAdminId { get; set; }
        public string ShopAppId { get; set; }
        public int OptionStatus { get; set; }
        public string BeginTime { get; set; }
        public string EndTime { get; set; }
        public string LoginName { get; set; }

    }
}
