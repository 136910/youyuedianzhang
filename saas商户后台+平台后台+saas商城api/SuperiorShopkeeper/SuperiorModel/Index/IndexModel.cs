﻿namespace SuperiorModel
{
    public class TotalIndexModel
    {
        public int TotalProductCount { get; set; }
        public int TotalUserCount { get; set; }
        public int TotalOrderCount { get; set; }
        public decimal TotalSallPrice { get; set; }
    }

    public class TodayModel
    {
        public decimal SallPrice { get; set; }
        public int PayOrderCount { get; set; }

        public int AddUserCount { get; set; }
        public int UserPayCount { get; set; }
    }
}
