﻿using System;
using System.Collections.Generic;

namespace SuperiorModel
{
    class WeiXinModel
    {
    }

    public class ShopAppCheckInfo
    {
        public ShopAppCheckInfo(int status,int auditid)
        {
            this.Status = status;
            this.Auditid = auditid;
            this.CreateTime = DateTime.Now;
        }
        /// <summary>
        /// 最近一次审核状态
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 最近一次审核编号
        /// </summary>
        public int Auditid { get; set; }
        /// <summary>
        /// 最近一次审核失败原因
        /// </summary>
        public string ErrorMsg { get; set; }

        public DateTime CreateTime { get; set; }

        public DateTime UpdateTime { get; set; }
    }

    public class ThirdCommitParams
    {
        /// <summary>
        /// 模板ID
        /// </summary>
        public int template_id { get; set; }
        /// <summary>
        /// 小程序配置JSON
        /// </summary>
        public string ext_json { get; set; }
        /// <summary>
        /// 版本号
        /// </summary>
        public string user_version { get; set; }
        /// <summary>
        /// 版本说明
        /// </summary>
        public string user_desc { get; set; }

    }

    public class ExtJosn
    {
        public ExtJosn()
        {
            this.extEnable = true;
            this.directCommit = false;
            this.ext = new ExtItem();
        }
        public bool extEnable { get; set; }
        public string extAppid { get; set; }
        public bool directCommit { get; set; }
        public ExtItem ext { get; set; }
    }
    public class ExtItem
    {
        public ExtItem()
        {
            this.host = "https://ws.youyue1118.com";
        }
        public string name { get; set; }
        public string host { get; set; }
        public string spid { get; set; }
        public string said { get; set; }

    }

    public class Wx_CategoryResponseModel
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        public List<Wx_CategoryItem> category_list { get; set; }
    }
    public class Wx_CategoryItem
    {
        public string first_class { get; set; }
        public string second_class { get; set; }
        public string third_class { get; set; }
        public int first_id { get; set; }
        public int second_id { get; set; }
        public int third_id { get; set; }

    }

    public class Wx_PagesResponseModel
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        public List<string> page_list { get; set; }
    }

    public class Wx_SubimitModel
    {
        public Wx_SubimitModel()
        {
            item_list = new List<Wx_SubimitItem>();
        }
        public List<Wx_SubimitItem> item_list { get; set; }
        public string feedback_info { get; set; }
        public string feedback_stuff { get; set; }

    }

    public class Wx_SubimitItem
    {
        public string address { get; set; }
        public string tag { get; set; }
        public string first_class { get; set; }
        public string second_class { get; set; }
        public string third_class { get; set; }
        public int first_id { get; set; }
        public int second_id { get; set; }
        public int third_id { get; set; }
        public string title { get; set; }

    }

    public class Wx_SubimitResultModel
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
        /// <summary>
        /// 提交返回得审核编号
        /// </summary>
        public int auditid { get; set; }
    }
}
