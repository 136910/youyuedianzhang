# 优越店长.net微商城Sass平台版本及.netcore独立商户部署版本

#### 如果对您有帮助，您可以点右上角 "Star" 支持一下，这样我们才有继续免费下去的动力，谢谢！

#### 介绍

1.  优越店长是一款基于.NET4.6开发的一款完整的微信商城SAAS平台，前端支持小程序、h5，由前端商城，商户管理后台，平台管理后台三大块组成，sass功能完善，支持商户拖拽式零代码创建并提交上传发布小程序，商城功能模块完善，用户管理，商品管理(支持多规格)，订单管理，运费模板管理，用户管理，营销管理等等。
2.  与此同时，也提供基于.netcore2.1开发的独立部署版本(非Saas)商城小程序端+管理后台。

#### Saas平台架构

1.  小程序接口项目：WEBAPI+REDIS+AUTOFAC
2.  商户后台：ASP.NET MVC+REDIS+AUTOFAC+Layui+JQUERY+部分VUE
3.  Saas平台管理后台:ASP.NET MVC+REDIS+AUTOFAC+Layui+JQUERY
4.  数据库:SQLSERVER 2008 R2
5.  服务器:windows server

#### 独立部署版(单商户)架构

1.  小程序接口项目:.netcore2.1+redis+swagger
2.  商户后台:ASP.NET MVC+REDIS+AUTOFAC+Layui+JQUERY
3.  数据库:SQLSERVER 2008 R2
4.  服务器:windows server/linux

#### 商城截图
![输入图片说明](https://images.gitee.com/uploads/images/2021/1029/161352_f123ebde_9923190.jpeg "p_3_gaitubao_1200x867.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2021/1029/155728_5bb905be_9923190.jpeg "p_.jpg")

![输入图片说明](https://images.gitee.com/uploads/images/2021/1029/155740_f1edec2b_9923190.jpeg "p_2.jpg")

#### 商户后台截图

![输入图片说明](https://images.gitee.com/uploads/images/2021/1116/095232_b0728d04_9923190.png "p_1.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/1116/095249_e277c57e_9923190.png "p_2.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/1116/095301_6dab55c4_9923190.png "p_3.png")

#### 平台后台截图

![输入图片说明](https://images.gitee.com/uploads/images/2021/1116/095316_3d334f08_9923190.png "p_1.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/1116/095327_2e9fc192_9923190.png "p_2.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/1116/095338_abaf9aac_9923190.png "p_3.png")

#### 项目演示

  官网:http://www.youyue1118.com

  商户后台：http://shop.youyue1118.com  体验账号:tyadmin 密码:12345678 勾选员工角色登录。

  小程序/h5商城演示：请微信搜索公众号 “优越店长”,菜单里有您需要的所有商城演示。

#### 部署说明

1.  手动创建数据库，使用数据库脚本执行创建表sql
2.  根据您的项目需求，使用VS2015以上版本打开项目，在相应项目config下的配置文件中，配置好所需配置，直接f5启动或者部署到IIS服务器启动。
3.  所有项目从.net framework 到.net core，均使用主流技术开发，.net开发工程师应非常容易的阅读并上手。
4.  关于为什么使用ado.net操作数据库并且使用存储过程，原因是创业项目，在最小开销下完成最大性能支撑。可自行进行二次开发使用EF,DAPPER等orm框架。



#### 联系我们

1.邮箱地址：324596966@qq.com  cqkjyydz@163.com

2.微信：wang324596966或13691031659

3.联系电话:13691031659


#### 使用声明

1.  允许个人学习研究使用，支持二次开发，允许商业用途（仅限自运营）。
2.  允许商业用途，但仅限自运营，如果商用必须保留版权信息，望自觉遵守。
3.  不允许对程序代码以任何形式任何目的的再发行或出售，否则将追究侵权者法律责任。
