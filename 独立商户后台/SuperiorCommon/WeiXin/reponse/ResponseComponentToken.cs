﻿namespace SuperiorCommon
{
    public class ResponseComponentToken : WxJsonResult
    {
        public string component_access_token
        {
            get;
            set;
        }

        public string expires_in
        {
            get;
            set;
        }
    }
}
