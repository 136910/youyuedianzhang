﻿using System.Web;

namespace SuperiorCommon
{
    public class SessionManager
    {
        /// <summary>
        /// 设置登录session，id参数，是针对同一个用户打开不同得两个商城或者后台得拼接seesionkey,主要针对H5商城设置得参数，因为H5商城得商城信息从url读取，用户信息从seesion读，必须得保证该用户是该商城得
        /// </summary>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        /// <param name="mins"></param>
        /// <param name="id"></param>
        public static void SetSession(string key, object obj, string id = "", int mins = 360)
        {
            if (id == "")
                HttpContext.Current.Session[key] = obj;
            else
                HttpContext.Current.Session[key + id] = obj;
            HttpContext.Current.Session.Timeout = mins;
        }

    }

    public class SessionKey
    {
        /// <summary>
        ///商户后台
        /// </summary>
        public static string ShopKey
        {
            get
            {
                return "YYDZ_CUSTOMER";
            }
        }
        
    }
}
