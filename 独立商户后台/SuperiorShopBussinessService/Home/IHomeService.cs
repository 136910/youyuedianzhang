﻿using SuperiorModel;

namespace SuperiorShopBussinessService
{
    public interface IHomeService:IService
    {
        HomeModel GetHomeData(int shopAdminId);
        Chart_AddUserModel GetAddUserChart(int shopAdminId);
        Chart_SallModel GetSallChart(int shopAdminId);
    }
}
