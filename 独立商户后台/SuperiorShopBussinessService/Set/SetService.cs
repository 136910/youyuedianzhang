﻿using System;
using System.Collections.Generic;
using SuperiorModel;
using SuperiorShopDataAccess;
using SuperiorCommon;
using System.Data;

namespace SuperiorShopBussinessService
{
    public class SetService : ISetService
    {
        public List<ShippingTemplateModel> GetShippingTemplateList(int shopAdminId)
        {
            var res = new List<ShippingTemplateModel>();
            var ds = SetDataAccess.GetShippingTemplateList(shopAdminId);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        res.Add(new ShippingTemplateModel()
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            ShippingTemplatesName = dr["ShippingTemplatesName"].ToString(),
                            ChargType = Convert.ToInt32(dr["ChargType"]),
                            CreateTime = Convert.ToDateTime(dr["CreateTime"])
                        });
                    }
                }
            }
            return res;
        }
        public int DelShippingTemplate(int id, out string msg)
        {
            var ret = SetDataAccess.DelShippingTemplate(id);
            msg = string.Empty;
            switch (ret)
            {
                case 2:
                    msg = "当前模板正在被商品使用";
                    break;
                default:
                    msg = "请求异常";
                    break;
            }
            return ret;
        }
        public ShippingTemplateModel GetShippingTemplateModel(int id)
        {
            var res = new ShippingTemplateModel();
            var ds = SetDataAccess.GetShippingTemplateModel(id);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    res.Id = Convert.ToInt32(dr["Id"]);
                    res.ShippingTemplatesName = dr["ShippingTemplatesName"].ToString();
                    res.ChargType = Convert.ToInt32(dr["ChargType"]);
                }
                if (DataManager.CheckHasRow(ds.Tables[1]))
                {
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        res.Items.Add(new ShippingTemplateItem() {
                            Id = Convert.ToInt32(dr["Id"]),
                            ShippingTemplateId = Convert.ToInt32(dr["ShippingTemplateId"]),
                            ProvinceIds = dr["ProvinceIds"].ToString(),
                            FirstValue = Convert.ToInt32(dr["FirstValue"]),
                            NextValue = Convert.ToInt32(dr["NextValue"]),
                            FirstAmount = DataManager.GetRowValue_Decimal(dr["FirstAmount"]),
                            NextAmount = DataManager.GetRowValue_Decimal(dr["NextAmount"]),
                            ProvinceStr = dr["ProvinceStr"].ToString(),
                        });
                    }
                }
            }
            return res;
        }

        public bool AddShippingTemplate(ShippingTemplateModel model)
        {
            foreach (var item in model.Items)
            {
                //由于不能参数化，要手工检测SQL注入
                if (ToolManager.IsSqlin(item.FirstAmount.ToString()) || ToolManager.IsSqlin(item.FirstValue.ToString()) || ToolManager.IsSqlin(item.NextAmount.ToString()) || ToolManager.IsSqlin(item.NextValue.ToString()) || ToolManager.IsSqlin(item.ProvinceIds) || ToolManager.IsSqlin(item.ProvinceStr))
                {
                    LogManger.Instance.WriteLog("店主后台ID为" + model.ShopAdminId + "，发生了SQL注入");
                    return false;
                }
            }
            return SetDataAccess.AddShippingTemplate(model);
        }

        public bool EditShippingTemplate(ShippingTemplateModel model)
        {
            foreach (var item in model.Items)
            {
                //由于不能参数化，要手工检测SQL注入
                if (ToolManager.IsSqlin(item.FirstAmount.ToString()) || ToolManager.IsSqlin(item.FirstValue.ToString()) || ToolManager.IsSqlin(item.NextAmount.ToString()) || ToolManager.IsSqlin(item.NextValue.ToString()) || ToolManager.IsSqlin(item.ProvinceIds) || ToolManager.IsSqlin(item.ProvinceStr))
                {
                    LogManger.Instance.WriteLog("店主后台ID为" + model.ShopAdminId + "，发生了SQL注入");
                    return false;
                }
            }
            return SetDataAccess.EditShippingTemplate(model);
        }

        public void Dispose()
        {

        }
    }
}
