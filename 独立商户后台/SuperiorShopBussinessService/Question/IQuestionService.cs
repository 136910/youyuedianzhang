﻿using SuperiorModel;

namespace SuperiorShopBussinessService
{
    public interface IQuestionService:IService
    {
        PagedSqlList<QuestionModel> SearchQuestionList(QuestionCriteira criteria);
    }
}
