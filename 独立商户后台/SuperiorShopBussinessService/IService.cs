﻿using System;

namespace SuperiorShopBussinessService
{
    /// <summary>
    /// 基础服务接口，释放资源
    /// </summary>
    public interface IService : IDisposable
    {
        void Dispose();
    }
}
