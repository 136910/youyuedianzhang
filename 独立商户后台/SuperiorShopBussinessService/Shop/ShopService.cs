﻿using SuperiorCommon;
using SuperiorModel;
using SuperiorShopDataAccess;
using System;
using System.Collections.Generic;
using System.Data;

namespace SuperiorShopBussinessService
{
    public class ShopService : IShopService
    {
        public void Dispose()
        {

        }

        public List<int> GetShopAppIdsByShopAdminId(int shopAdminId)
        {
            var res = new List<int>();
            var ds = ShopDataAccess.GetShopAppIdsByShopAdminId(shopAdminId);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        res.Add(Convert.ToInt32(dr["Id"])
                        );
                    }
                }
            }
            return res;
        }

        
        public ShopInfoModel GetShopModel(string username, string password)
        {
            var res = new ShopInfoModel();
            var ds = ShopDataAccess.GetShopModel(username, password);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    var dr = ds.Tables[0].Rows[0];
                    res.Id = SecretClass.EncryptQueryString(dr["Id"].ToString());
                    res.LoginName = username;
                    res.PassWord = dr["PassWord"].ToString();
                    res.Introduce = dr["Introduce"].ToString();
                    res.PhoneNumber = dr["PhoneNumber"].ToString();
                    res.QqCode = dr["QqCode"].ToString();
                    res.WxCode = dr["WxCode"].ToString();
                    res.Contact = dr["Contact"].ToString();
                    res.OptionStatus = Convert.ToInt32(dr["OptionStatus"]);
                    res.SerectId = SecretClass.EncryptQueryString(dr["Id"].ToString());
                }
            }
            return res;
        }
        public ShopInfoModel GetShopModel_Role(string username, string password)
        {
            var res = new ShopInfoModel();
            var ds = ShopDataAccess.GetShopModel_Role(username, password);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    var dr = ds.Tables[0].Rows[0];
                    res.Id = SecretClass.EncryptQueryString(dr["ShopAdminId"].ToString());
                    res.LoginName = username;
                    res.PassWord = dr["PassWord"].ToString();
                    res.Introduce = dr["Introduce"].ToString();
                    res.PhoneNumber = dr["PhoneNumber"].ToString();
                    res.QqCode = dr["QqCode"].ToString();
                    res.WxCode = dr["WxCode"].ToString();
                    res.Contact = dr["Contact"].ToString();
                    res.OptionStatus = Convert.ToInt32(dr["AdminOptionStatus"]);
                    res.SerectId = SecretClass.EncryptQueryString(dr["Id"].ToString());
                    res.ShopAdminType = 2;
                    res.ShopLoginRoleModel.Account = dr["Account"].ToString();
                    res.ShopLoginRoleModel.Authoritys = dr["Authoritys"].ToString();
                    res.ShopLoginRoleModel.OptionStatus = Convert.ToInt32(dr["RoleOptionStatus"]);
                }
            }
            return res;
        }

        public int InsertShop(ShopInfoModel model, out string msg)
        {
            model.PassWord = MD5Manager.MD5Encrypt(model.PassWord);
            var ret = ShopDataAccess.InsertShop(model);
            msg = string.Empty;
            switch (ret)
            {
                case 2:
                    msg = "用户名已存在";
                    break;
                case 3:
                    msg = "手机号已经存在";
                    break;
                default:
                    msg = "请求异常";
                    break;
            }
            return ret;
        }


    }
}
