﻿using SuperiorCommon;
using SuperiorModel;
using SuperiorShopDataAccess;
using System;
using System.Collections.Generic;
using System.Data;

namespace SuperiorShopBussinessService
{
    public class OrderService : IOrderService
    {
        public PagedSqlList<OrderModel> Search(OrderCriteria criteria)
        {
            var queryList = new List<OrderModel>();
            var totalCount = 0;
            var ds = OrderDataAccess.Search(criteria);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new OrderModel()
                        {
                            Id = SecretClass.EncryptQueryString(dr["Id"].ToString()),
                            OrderNo = dr["OrderNo"].ToString(),
                            ShopName = dr["ShopName"].ToString(),
                            NickName = dr["NickName"].ToString(),
                            ServiceStatus = Convert.ToInt32(dr["ServiceStatus"]),
                            RealityAmount = Convert.ToDecimal(dr["RealityAmount"]),
                            OrderStatus = Convert.ToInt32(dr["OrderStatus"]),
                            CreateTime = Convert.ToDateTime(dr["CreateTime"]),
                            LoginName = dr["LoginName"].ToString(),
                            FailReason = dr["FailReason"].ToString()
                        });
                    }
                }
                totalCount = Convert.ToInt32(ds.Tables[1].Rows[0]["totalCount"]);
            }
            return new PagedSqlList<OrderModel>(queryList, criteria.PagingResult.PageIndex, criteria.PagingResult.PageSize, totalCount);
        }

        public OrderDetailModel GetDetail(int orderid)
        {
            var res = new OrderDetailModel();
            var ds = OrderDataAccess.GetDetail(orderid);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    res.OrderNo = dr["OrderNo"].ToString();
                    res.ConsigneeName = dr["ConsigneeName"].ToString();
                    res.ConsigneePhone = dr["ConsigneePhone"].ToString();
                    res.ZipCode = dr["ZipCode"].ToString();
                    res.Address = dr["Address"].ToString();
                    res.PayOderNo = dr["PayOderNo"].ToString();
                    res.RealityAmount = dr["RealityAmount"].ToString();
                    res.Remark = dr["Remark"].ToString();
                    res.FailReason = dr["FailReason"].ToString();
                    res.CreateTime = Convert.ToDateTime(dr["CreateTime"]);
                    res.PayTime = dr["PayTime"].ToString();
                    res.SendTime = dr["SendTime"].ToString();
                    res.ServiceTime = dr["ServiceTime"].ToString();
                    res.OrderStatus = Convert.ToInt32(dr["OrderStatus"]);
                    res.SendStatus = Convert.ToInt32(dr["SendStatus"]);
                    res.ServiceStatus = Convert.ToInt32(dr["ServiceStatus"]);
                    res.Id = SecretClass.EncryptQueryString(dr["Id"].ToString());
                    res.OrderAmount = DataManager.GetRowValue_Decimal(dr["OrderAmount"]);
                    res.CouponDelAmount = DataManager.GetRowValue_Decimal(dr["CouponDelAmount"]);
                    res.ShippingAmount = DataManager.GetRowValue_Decimal(dr["ShippingAmount"]);
                    res.CouponId = DataManager.GetRowValue_Int(dr["CouponId"]);
                    res.CouponName = dr["CouponName"].ToString();
                    res.DeliveryName = dr["DeliveryName"].ToString();
                    res.DeliveryNo = dr["DeliveryNo"].ToString();
                }
                if (DataManager.CheckHasRow(ds.Tables[1]))
                {
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        res.OrderItems.Add(new OrderItem() {
                            ProductName = dr["ProductName"].ToString(),
                            ProppetyCombineName = dr["ProppetyCombineName"].ToString(),
                            SkuCode = dr["SkuCode"].ToString(),
                            BuyNum = Convert.ToInt32(dr["BuyNum"]),
                            SalePrice = Convert.ToDecimal(dr["SalePrice"])
                        });
                    }
                }

            }
            return res;
        }

        public void SendProduct(SendProductParms model)
        {
            OrderDataAccess.SendProduct(model);
        }
        public void UpdateDelivery(SendProductParms model)
        {
            OrderDataAccess.UpdateDelivery(model);
        }
        public void FinishService(int orderid)
        {
            OrderDataAccess.FinishService(orderid);
        }


       
        public void Dispose()
        {

        }
    }
}
