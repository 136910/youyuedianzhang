﻿using SuperiorShopBussinessService;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SuperiorCommon;
using SuperiorModel;

namespace SuperiorShopkeeper.Controllers
{
    [JsonException]
    [CheckLoginFitler]
    public class ShopAdminController : BaseController
    {
        private readonly IShopAdminService _IShopAdminService;
        private readonly IShopService _IShopService;

        public ShopAdminController(IShopAdminService IShopAdminService, IShopService IShopService)
        {
            _IShopAdminService = IShopAdminService;
            _IShopService = IShopService;
        }
        protected override void Dispose(bool disposin)
        {
            this._IShopAdminService.Dispose();
            this._IShopService.Dispose();
            base.Dispose(disposin);
        }
        // GET: ShopAdmin
        [ShopAuthority(AuthorityMenuEnum.ShopAdmin)]
        public ActionResult ShopAdmin()
        {
            var model = _IShopAdminService.GetShopAdminModel(UserContext.DeSecretId.ToInt());
            return View(model);
        }
        /// <summary>
        /// 编辑商户信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        [ShopAuthority(AuthorityOptionEnum.EditShopAdminInfo)]
        public JsonResult EditShopAdminInfo(ShopInfoModel model)
        {
            if (string.IsNullOrEmpty(model.Contact) || string.IsNullOrEmpty(model.Introduce))
                return Error("联系人或商户信息不能为空");
            model.Id = UserContext.DeSecretId;
            _IShopAdminService.EditShopAdmin(model);
            return Success(true);
        }

        public ActionResult EditPassWord()
        {
            return View();
        }
        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        public JsonResult EditPw(EditPassWordModel model)
        {
            if (!ModelState.IsValid)
                return Error(base.ErrorMsg());
            if (model.PassWord != model.ResetPassWord)
                return Error("两次密码不一致");
            if (!ValidateUtil.IsValidPassword25(model.PassWord))
                return Error("密码6-25位的字母或数字组合");
            var pw = MD5Manager.MD5Encrypt(model.PassWord);
            if (UserContext.User.PassWord != MD5Manager.MD5Encrypt(model.OldPassWord))
                return Error("旧密码输入错误");

            _IShopAdminService.EditPassWord(UserContext.DeSecretId.ToInt(), pw);
            var shop = UserContext.User;
            shop.PassWord = pw;
            //重新更新session模型信息
            SessionManager.SetSession(SessionKey.ShopKey, shop);
            return Success(true);
        }
        /// <summary>
        /// 角色管理
        /// </summary>
        /// <returns></returns>
        /// 
        [ShopAuthority(AuthorityMenuEnum.RoleList)]
        public ActionResult RoleList()
        {
            var model = _IShopAdminService.GetRoleList(UserContext.DeSecretId.ToInt());
            return View(model);
        }
        /// <summary>
        /// 天加角色
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// 
        [AjaxOnly]
        [HttpPost]
        [ShopAuthority(AuthorityOptionEnum.AddRole)]
        public JsonResult AddRole(ShopRoleModel model)
        {
            if (!ModelState.IsValid)
                return Error(base.ErrorMsg());
            model.ShopAdminId = UserContext.DeSecretId.ToInt();
            string msg = "";
            var ret = _IShopAdminService.AddShopRole(model, out msg);
            if (ret != 1)
                return Error(msg);
            return Success(true);
        }
        /// <summary>
        /// 修改角色
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// 
        [AjaxOnly]
        [HttpPost]
        [ShopAuthority(AuthorityOptionEnum.EditRole)]
        public JsonResult EditRole(ShopRoleModel model)
        {
            if (!ModelState.IsValid)
                return Error(base.ErrorMsg());
            model.ShopAdminId = UserContext.DeSecretId.ToInt();
            string msg = "";
            var ret = _IShopAdminService.EditShopRole(model, out msg);
            if (ret != 1)
                return Error(msg);
            return Success(true);
        }
        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        [ShopAuthority(AuthorityOptionEnum.DeleteRole)]
        public JsonResult DeleteRole(int id)
        {
            string msg = "";
            var ret = _IShopAdminService.DelShopRole(id, out msg);
            if (ret != 1)
                return Error(msg);
            return Success(true);
        }
        /// <summary>
        /// 当前角色的权限设置
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult CurrentRoleAuthority(int id)
        {
            var model = _IShopAdminService.GetRole(id);
            if (model.AuthoritysList.Count == 0)
            {
                model.AuthoritysList = AuthorityController.InitAuthorityList;
            }
            else
            {
                //判断是否有新加入的权限，有的话，用户没更新，给加上，默认没权限,如果有新删除的权限，也更新
                var allList = AuthorityController.InitAuthorityList;
                var _tempList = new List<AuthorityModel>();
                foreach (var item in allList)
                {
                    if (model.AuthoritysList.Any(p => p.id == item.id))
                    {
                        _tempList.Add(model.AuthoritysList.Where(i => i.id == item.id).FirstOrDefault());
                    }
                    else
                    {
                        //不存在，添加
                        item.checkArr[0].isChecked = "0";
                        _tempList.Add(item);
                    }
                }
                model.AuthoritysList = _tempList;
            }
            return View(model);

        }
        /// <summary>
        /// 设置角色权限
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        [ShopAuthority(AuthorityOptionEnum.SetRoleAuthority)]
        public JsonResult SetRoleAuthority(ShopRoleModel model)
        {
            _IShopAdminService.SetShopRoleAuthority(model);
            return Success(true);
        }
        /// <summary>
        /// 员工账户管理
        /// </summary>
        /// <returns></returns>
        /// 
        [ShopAuthority(AuthorityMenuEnum.ShopAccountList)]
        public ActionResult ShopRoleAccountList()
        {
            var model = _IShopAdminService.GetRoleAccountListDs(UserContext.DeSecretId.ToInt());
            return View(model);
        }

        /// <summary>
        /// 删除员工账号
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        [ShopAuthority(AuthorityOptionEnum.DeleteRoleAccount)]
        public JsonResult DeleteRoleAccount(int id)
        {
            _IShopAdminService.DelShopRoleAccount(id);
            return Success(true);
        }

        /// <summary>
        /// 操作员工账号
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        [ShopAuthority(AuthorityOptionEnum.OptionAccount)]
        public JsonResult OptionAccount(int id, int optionStatus)
        {
            _IShopAdminService.OptionShopRoleAccount(id, optionStatus);
            return Success(true);
        }
        /// <summary>
        /// 添加或修改账号的页面
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult AddOrEditAccount(int id = 0)
        {
            var model = new ShopRoleAccountModel();
            if (id != 0)
                model = _IShopAdminService.GetRoleAccountDs(id);
            var roles = _IShopAdminService.GetRoleList(UserContext.DeSecretId.ToInt());
            ViewBag.Roles = roles;
            return View(model);

        }
        /// <summary>
        /// 添加或修改员工账户
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        [ShopAuthority(AuthorityOptionEnum.AddOrEditRoleAccount)]
        public JsonResult AddOrEditRoleAccount(ShopRoleAccountModel model)
        {

            if (!ModelState.IsValid)
                return Error(base.ErrorMsg());
            model.ShopAdminId = UserContext.DeSecretId.ToInt();
            model.PassWord = MD5Manager.MD5Encrypt(model.PassWord);
            string msg = "";
            int ret = 1;
            if (model.Id == 0)
                ret = _IShopAdminService.AddShopRoleAccount(model, out msg);
            else
                ret = _IShopAdminService.EditShopRoleAccount(model, out msg);
            if (ret != 1)
                return Error(msg);
            return Success(true);
        }
 

    }
}