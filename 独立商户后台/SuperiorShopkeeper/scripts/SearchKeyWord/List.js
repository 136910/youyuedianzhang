﻿(function () {
    SearchKeyWordListClass = {};
    SearchKeyWordListClass.Instance = {
        Init: function () {
            $(document).on('click', '#btn_addKeyWord', this.Add);
            $(document).on('click', '.DeleteKeyWord', this.Delete);
            $(document).on('click', '.OptionKeyWord', this.OptionStatus);
            //layui验证表单写法-----begin
            var form;
            layui.use(['form'], function () {
                form = layui.form;
                form.on('submit(btn_submit)', SearchKeyWordListClass.Instance.Submit);
            });
            //-----end
        },
        Add: function () {
            layer.open({
                type: 1,
                skin: 'layui-layer-rim', //加上边框
                area: ['420px', '240px'], //宽高
                content: SearchKeyWordListClass.Instance.CreateHtml()

            });
        },
        OptionStatus: function () {
            var id = $(this).attr("OptionId");
            var opt = $(this).attr("Opt");
            var index = layer.load(1);
            $.get("/SearchWord/Option?id=" + id + "&optionStatus=" + opt + "&r=" + new Date().getTime(), function (data) {
                layer.close(index);
                if (data.IsSuccess) {
                    layer.alert("执行成功", function () {
                        window.location.href = "/SearchWord/List";
                    })

                } else {
                    layer.alert(data.Message);
                }
            })
        },
        Delete: function () {
            var id = $(this).attr("OptionId");
            layer.confirm('删除后数据不可恢复，确定要执行吗？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                var index = layer.load(1);
                $.get("/SearchWord/Delete?id=" + id + "&r=" + new Date().getTime(), function (data) {
                    layer.close(index);
                    if (data.IsSuccess) {
                        layer.alert("执行成功", function () {
                            window.location.href = "/SearchWord/List";
                        })

                    } else {
                        layer.alert(data.Message);
                    }
                })
            }, function () {

            });
        },
        Submit: function () {
            var KeyWordName = $("#text_keyword").val();
            var model = {};
            model.KeyWord = KeyWordName;
            var index = layer.load(1);
            RequestManager.Ajax.Post("/SearchWord/Add", model, true, function (data) {
                layer.close(index);
                if (data.IsSuccess) {
                    layer.alert("执行成功", function () {
                        window.location.href = "/SearchWord/List";
                    })

                } else {
                    layer.alert(data.Message);
                }
            })
        },
        CreateHtml: function () {
            var arr = [];
            arr.push('<div class="layui-form" action="" >');
            arr.push('<div class="layui-form-item">');
            arr.push('<label for="text_keyword" class="layui-form-label">搜索词 </label>');
            arr.push('<div class="layui-input-inline" style="width:50%">');
            arr.push('<input type="text" id="text_keyword" name="text_keyword" lay-verify="required" class="layui-input" placeholder="请输入搜索词">');
            arr.push('</div>');
            arr.push(' <div class="layui-form-mid layui-word-aux"><span class="x-red">*</span></div>');
            arr.push('</div>');
            arr.push('<div class="layui-form-item" style="text-align:center;">');
            arr.push('<div class="layui-input-block" style="margin:0;"><button class="layui-btn" lay-submit lay-filter="btn_submit">确定</button></div>');
            arr.push('</div>');
            arr.push('</div>');
            return arr.join('');
        }
    };
})();