﻿using System.Text;
using SuperiorSqlTools;
using System.Data;
using System.Data.SqlClient;

namespace SuperiorShopDataAccess
{
    public class HomeDataAccess:BaseDataAccess
    {
        public static DataSet GetHomeData(int shopAdminId)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAdminId",shopAdminId)
            };
            var sb = new StringBuilder();
            sb.Append("select COUNT(*)as TotalProductCount from C_Product where IsDel=0 and ShopAdminId=@shopAdminId;");
            sb.Append("select COUNT(*)as TotalUserCount from C_Users where  ShopAdminId=@shopAdminId;");
            sb.Append("select COUNT(*)as TotalOrderCount from C_Order where OrderStatus=1 and ShopAdminId=@shopAdminId;");
            sb.Append("select SUM(RealityAmount) as TotalOrderAmount from C_Order where OrderStatus=1 and ShopAdminId=@shopAdminId;");
            sb.Append("select COUNT(*) as WaitSendCount from C_Order where ShopAdminId=@shopAdminId and OrderStatus=3;");
            sb.Append("select COUNT(*) as WaitServiceCount from C_Order where ShopAdminId=@shopAdminId and OrderStatus=4 and ServiceStatus=0;");
            return sqlManager.ExecuteDataset(CommandType.Text, sb.ToString(), parms);
        }


        public static DataSet GetAddUserChart(int shopAdminId)
        {
            var sqlManager = new SqlManager();
            var dateArea = GetDataArea(1,7);
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAdminId",shopAdminId),
                new SqlParameter("@mindate",dateArea.MinDate),
                new SqlParameter("@maxdate",dateArea.MaxDate)
            };
            var sql = "select CreateTime from C_Users where ShopAdminId=@shopAdminId and CreateTime >=@mindate and CreateTime<=@maxdate";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }
        public static DataSet GetSallChart(int shopAdminId)
        {
            var sqlManager = new SqlManager();
            var dateArea = GetDataArea(1, 7);
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAdminId",shopAdminId),
                 new SqlParameter("@mindate",dateArea.MinDate),
                new SqlParameter("@maxdate",dateArea.MaxDate)
            };
            //sql含义：取出当前店铺，近7日按照下单日期分组的总购买量，总金额。
            var sql = " select SUM(t.RealityAmount) as TotalAmount,SUM(t.BuyNum) as TotalCount,CreateTime from(select RealityAmount,CreateTime,(select SUM(BuyNum) from C_OrderDetails where OrderId=o.Id)as BuyNum from C_Order  o with(nolock) where ShopAdminId=@shopAdminId and CreateTime >=@mindate and CreateTime<=@maxdate)t group by CreateTime";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }
   

    }
}
