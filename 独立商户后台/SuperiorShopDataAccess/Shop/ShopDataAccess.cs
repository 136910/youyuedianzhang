﻿using SuperiorSqlTools;
using System.Data;
using System.Data.SqlClient;
using SuperiorModel;

namespace SuperiorShopDataAccess
{
    public class ShopDataAccess
    {
        public static DataSet GetShopModel(string username, string password)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@username",username),
                new SqlParameter("@password",password)
            };
            var sql = "select * from C_ShopAdmin where LoginName=@username and PassWord=@password";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public static DataSet GetShopAppIdsByShopAdminId(int shopAdminId)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAdminId",shopAdminId)
            };
            var sql = "seleCt Id from C_ShopApp where ShopAdminId=@shopAdminId";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }


        public static DataSet GetShopModel_Role(string username, string password)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@username",username),
                new SqlParameter("@password",password)
            };
            var sql = "select sra.*,sa.*,sr.RoleName,sr.Authoritys,sra.OptionStatus as RoleOptionStatus,sa.OptionStatus as AdminOptionStatus,sa.Id as ShopAdminId  from C_ShopRoleAccount sra left join C_ShopRole sr on sra.RoleId=sr.Id left join C_ShopAdmin sa on sra.ShopAdminId=sa.Id where sra.Account=@username and sra.PassWord=@password and sra.IsDel=0";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public static int InsertShop(ShopInfoModel model)
        {
            var sqlManager = new SqlManager();
            SqlParameter rtn_err = sqlManager.GetRtnParameter();
            SqlParameter[] parms =
            {
                new SqlParameter("@LoginName", model.LoginName),
                new SqlParameter("@PassWord",model.PassWord),
                new SqlParameter("@Introduce",model.Introduce),
                new SqlParameter("@PhoneNumber",model.PhoneNumber),
                new SqlParameter("@qqCode",model.QqCode??""),//为什么不会自动转化曹
                new SqlParameter("@WxCode",model.WxCode??""),
                new SqlParameter("@Contact",model.Contact),
                rtn_err
            };
            sqlManager.ExecuteNonQuery(CommandType.StoredProcedure, "InsertShop", parms);
            if (rtn_err.Value != null)
            {
                return int.Parse(rtn_err.Value.ToString());
            }
            return -1;
        }

       

       
        public static void UpdateShopAdmin(string openid, string nickname, string headImgUrl, int id)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@openid", openid),
                new SqlParameter("@nickname",nickname),
                new SqlParameter("@headImgUrl",headImgUrl),
                new SqlParameter("@id",id)

            };
            string sql = "update C_ShopAdmin set OpenId = @openid,HeadImgUrl=@headImgUrl,NickName=@nickname where Id= @id";
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }

        

    }
}
