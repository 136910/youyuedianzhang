﻿using System.Collections.Generic;
using SuperiorModel;
using SuperiorSqlTools;
using System.Data;
using System.Data.SqlClient;

namespace SuperiorShopDataAccess
{
    public class QuestionDataAccess
    {
        public static DataSet SearchQuestionList(QuestionCriteira criteria)
        {
            var sqlManager = new SqlManager();
            List<SqlParameter> parms = new List<SqlParameter>();
            var sb = sqlManager.CreateSb();
            if (!string.IsNullOrEmpty(criteria.ShopName))
            {
                sb.Append(" and s.ShopName=@shopname");
                parms.Add(new SqlParameter("@shopname", criteria.ShopName));
            }

            if (!string.IsNullOrEmpty(criteria.LoginName))
            {
                sb.Append(" and a.LoginName=@LoginName ");
                parms.Add(new SqlParameter("@LoginName", criteria.LoginName));
            }

            var sql = string.Format("select top({0}) * from(select ROW_NUMBER() over(order by q.Id desc)as rownum,q.*,a.LoginName,s.ShopName, u.NickName from C_Question q left join C_ShopAdmin a on q.ShopAdminId=a.Id left join C_ShopApp s on q.ShopAppId=s.Id  left join C_Users u on q.UserId=u.Id where {1} )tt where tt.rownum>{2};select count(*) as totalCount from  C_Question q left join C_ShopAdmin a on q.ShopAdminId=a.Id left join C_ShopApp s on q.ShopAppId=s.Id  left join C_Users u on q.UserId=u.Id where {1}", criteria.PagingResult.PageSize, sb.ToString(), criteria.PagingResult.PageSize * criteria.PagingResult.PageIndex);
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms.ToArray());
        }

        public static void Insert(QuestionParms model)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                 new SqlParameter("@ShopAppId", model.ShopAppId),
                     new SqlParameter("@ShopAdminId", model.ShopAdminId),
                         new SqlParameter("@UserId", model.UserId),
                             new SqlParameter("@Question", model.Question)
            };
            var sql = "insert into C_Question(ShopAppId,ShopAdminId,UserId,Question)values(@ShopAppId,@ShopAdminId,@UserId,@Question)";
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }
    }
}
