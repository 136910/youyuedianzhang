﻿using System.ComponentModel.DataAnnotations;

namespace SuperiorModel
{
    public class ShopAdminModel
    {

    }

    public class EditPassWordModel
    {
        [Required(ErrorMessage = "旧密码不能为空")]
        public string OldPassWord { get; set; }
        [Required(ErrorMessage = "新密码不能为空")]
        public string PassWord { get; set; }
        [Required(ErrorMessage = "再次密码不能为空")]
        public string ResetPassWord { get; set; }
    }
}
