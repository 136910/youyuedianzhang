const app = getApp();
//app.initConfig(app);
Page({

  /**
   * 页面的初始数据
   */
  data: {
    banners: null,
    hots: null,
    commands: null,
    menus: null,
    specialPlace: null,
    specialArea: null,
    words: null,
    isexpress: 0,
    headerTemplate: null, //顶部组合组件
    templates: [] //body中组件数组
  },
  jumpMenu: function (e) {
    var menuid = e.currentTarget.dataset.menuid;
    app.globalData.menuid = menuid;
    wx.switchTab({
      url: '/pages/menu/menulist',
    })
  },
  spanSearch: function (e) {
    var keyword = e.currentTarget.dataset.hot;
    app.globalData.keyword = keyword;
    wx.switchTab({
      url: '/pages/menu/menulist',
    })
  },
  keyWordChange: function (ev) {
    this.setData({
      keykeywoword: ev.detail.value
    });
  },
  btnSearch: function () {
    app.globalData.keyword = this.data.keyword;
    wx.switchTab({
      url: '/pages/menu/menulist',
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    //wx.hideShareMenu();
    var that = this;

    app.initConfig().then(function (res) {
      if (res == 200) {
        that.checkShop();
        wx.setNavigationBarTitle({
          title: app.appSetting.name //修改title
        })
      }

    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    /*
      var that = this;
    app.initConfig().then(function (res) {
      if (res == 200) {
        that.getBanners();
        that.getHots();
        that.getCommands();
        that.getMenus();
        that.getSpecialPlace();
        that.getSpecialArea();
        that.getHotWords();
      }

    })
    */


  },
  checkShop: function () {
    var that = this;
    wx.request({
      url: app.appSetting.host + "/api/Home/CheckShop?spid=" + app.appSetting.spid + "&apptype=1",
      data: {},
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log(res);
        if (res.data.Code = 200 && res.data.Message == '') {
          that.setData({
            isexpress: res.data.Data ? 1 : 0
          })
          app.globalData.isExpress = res.data.Data ? 1 : 0;
          if (!res.data.Data)
            that.initData();
        } else {
          wx.showToast({
            title: res.data.Message,
            icon: 'none'
          })
        }
      }
    })
  },
  getHotWords: function () {
    var that = this;
    wx.request({
      url: app.appSetting.host + "/Api/Home/GetSearchKeyword?said=" + app.appSetting.said,
      data: {},
      method: 'GET',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log(res);
        if (res.data.Code = 200 && res.data.Message == '') {
          if (res.data.Data.length >= 0) {
            that.setData({
              words: res.data.Data
            })
          }
        } else {
          wx.showToast({
            title: res.data.Message,
            icon: 'none'
          })
        }
      }
    })
  },
  getMenus: function () {
    var that = this;
    wx.request({
      url: app.appSetting.host + "/api/Product/GetMenuList?said=" + app.appSetting.said,
      data: {},
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log(res);
        if (res.data.Code = 200 && res.data.Message == '') {
          if (res.data.Data.length >= 4) {
            that.setData({
              menus: res.data.Data
            })
          }

        } else {
          wx.showToast({
            title: res.data.Message,
            icon: 'none'
          })
        }
      }
    })
  },
  getSpecialArea: function () {
    var that = this;
    wx.request({
      url: app.appSetting.host + "/Api/Product/GetSpecialPlaceData?said=" + app.appSetting.said + "&type=2",
      data: {},
      method: "GET",
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        if (res.data.Code == 200 && res.data.Message == '') {
          if (res.data.Data.length > 0) {
            that.setData({
              specialArea: res.data.Data
            })
          }
        } else {
          wx.showToast({
            title: res.data.Message,
            icon: 'none'
          })
        }
      }
    })
  },
  getSpecialPlace: function () {
    var that = this;
    wx.request({
      url: app.appSetting.host + "/api/Product/GetSpecialPlaceData?said=" + app.appSetting.said + "&type=1",
      data: {},
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log(res);
        if (res.data.Code == 200 && res.data.Message == '') {
          if (res.data.Data.length > 0) {
            that.setData({
              specialPlace: res.data.Data
            })
          }


        } else {
          wx.showToast({
            title: res.data.Message,
            icon: 'none'
          })
        }
      }
    })
  },
  getIndexData: function () {
    var that = this;
    wx.showLoading({
      title: '读取中 ',
    })
    wx.request({
      //url: app.appSetting.host + "/api/MobileMall/index?ShopID=1AD227F46DFB1666",
      url: app.appSetting.host + "/api/Home/GetHomeData?spid=" + app.appSetting.spid + "&said=" + app.appSetting.said,
      data: {},
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log(res);
        wx.hideLoading();
        if (res.data.Code = 200 && res.data.Message == '') {
          that.setData({
            mallIndexData: res.data.Data,
          })
        } else {
          wx.showToast({
            title: res.data.Message,
            icon: 'none'
          })
        }
      }
    })
  },
  getBanners: function () {
    var that = this;
    wx.showLoading({
      title: '读取中... ',
    })
    wx.request({
      //url: app.appSetting.host + "/api/MobileMall/index?ShopID=1AD227F46DFB1666",
      url: app.appSetting.host + "/api/Home/GetBanners?spid=" + app.appSetting.spid + "&said=" + app.appSetting.said,
      data: {},
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log(res);
        wx.hideLoading();
        if (res.data.Code == 200 && res.data.Message == '') {
          that.setData({
            banners: res.data.Data,
          })
        } else {
          wx.showToast({
            title: res.data.Message,
            icon: 'none'
          })
        }
      }
    })
  },

  getHots: function () {
    var that = this;
    wx.showLoading({
      title: '读取中... ',
    })
    wx.request({
      //url: app.appSetting.host + "/api/MobileMall/index?ShopID=1AD227F46DFB1666",
      url: app.appSetting.host + "/api/Home/GetHotProducts?spid=" + app.appSetting.spid + "&said=" + app.appSetting.said,
      data: {},
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log(res);
        wx.hideLoading();
        if (res.data.Code == 200 && res.data.Message == '') {
          that.setData({
            hots: res.data.Data,
          })
        } else {
          wx.showToast({
            title: res.data.Message,
            icon: 'none'
          })
        }
      }
    })
  },

  getCommands: function () {
    var that = this;
    wx.showLoading({
      title: '读取中... ',
    })
    wx.request({
      //url: app.appSetting.host + "/api/MobileMall/index?ShopID=1AD227F46DFB1666",
      url: app.appSetting.host + "/api/Home/GetCommandProducts?spid=" + app.appSetting.spid + "&said=" + app.appSetting.said,
      data: {},
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log(res);
        wx.hideLoading();
        if (res.data.Code == 200 && res.data.Message == '') {
          that.setData({
            commands: res.data.Data,
          })
        } else {
          wx.showToast({
            title: res.data.Message,
            icon: 'none'
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.initData();
  },

  initData: function () {
    var that = this;
    wx.showLoading({
      title: '读取中... ',
    })
    wx.request({
      url: app.appSetting.host + "/api/Decorate/GetDecorateDesign?spid=" + app.appSetting.spid + "&said=" + app.appSetting.said,
      data: {},
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log(res);
        wx.hideLoading();
        if (res.data.Code == 200 && res.data.Message == '') {
          //找出顶部组件
          var _headerComponent = that.findHeaderComponent(res.data.Data.DecorateDesign);
          console.log(res.data.Data.DecorateDesign);
          //获得BODY组件数组
          var _bodyComonents = that.makeBodyComponentArr(res.data.Data.DecorateDesign);
          that.setData({
            templates: _bodyComonents,
            headerTemplate: _headerComponent
          })
          //按需加载数据
          that.requiredData(_bodyComonents, _headerComponent);
        } else {
          wx.showToast({
            title: res.data.Message,
            icon: 'none'
          })
        }
      }
    })

  },

  findHeaderComponent: function (list) {
    var res = null;
    for (var i = 0; i < list.length; i++) {
      if (list[i].Name == 'head') {
        res = list[i];
        break;
      }
    }
    return res;
  },

  makeBodyComponentArr: function (list) {
    var res = [];
    for (var i = 0; i < list.length; i++) {
      if (list[i].Name != 'head') {
        res.push(list[i]);
      }
    }
    return res;
  },

  requiredData: function (items, header) {
    var that = this;
    if (header.IsRender) {
      this.getBanners();
      this.getHotWords();
      //改变顶部颜色
      if(header.Color!='')
        wx.setNavigationBarColor({
          backgroundColor: header.Color,
          frontColor: '#ffffff'
        })
    }
    items.forEach(item => {
      if (item.Name == 'keyword' && item.IsRender && !header.IsRender)
        that.getHotWords();
      if (item.Name == 'banner' && item.IsRender && !header.IsRender)
        that.getBanners();
      if (item.Name == 'menu' && item.IsRender)
        that.getMenus();
      if (item.Name == 'productSpecial' && item.IsRender)
        that.getSpecialPlace();
      if (item.Name == 'productArea' && item.IsRender)
        that.getSpecialArea();
      if (item.Name == 'productHot' && item.IsRender)
        that.getHots();
      if (item.Name == 'productCommand' && item.IsRender)
        that.getCommands();

    });
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  goToGoodsList() {
    wx.navigateTo({
      url: "/pages/menu/menu?menuid=0"
    });
  }
})